﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DvdLibrary.DATA.Config;

namespace DvdLibrary.TESTS
{
    static class Utilities
    {
        internal static void RebuildTestDVd()
        {
            //string script = new FileInfo("Scripts/RebuildTestDB.txt").OpenText().ReadToEnd();
            using (var cn = new SqlConnection(Settings.ConnectionString))
            {
                var cmd = new SqlCommand
                {
                    CommandText = new FileInfo("Scripts/RebuildTestDB.txt").OpenText().ReadToEnd(),
                    Connection = cn
                };

                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }
    }
}
