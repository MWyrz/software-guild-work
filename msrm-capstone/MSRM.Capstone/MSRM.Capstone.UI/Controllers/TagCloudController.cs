﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MSRM.Capstone.BLL;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.UI.Controllers
{
    public class TagCloudController : ApiController
    {
        // GET: api/TagCloud
        public IEnumerable<string> Get()
        {
            var manager = new TagManager();
            var values = manager.CountTags().Values.ToList();
            var keys = manager.CountTags().Keys.ToList();
            var list = new List<string>();
            foreach (var key in keys)
            {
                list.Add(key.Name);
            }
            foreach (var value in values)
            {
                list.Add(value.ToString());
            }
            return list;
        }

        // GET: api/TagCloud/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/TagCloud
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/TagCloud/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TagCloud/5
        public void Delete(int id)
        {
        }
    }
}
