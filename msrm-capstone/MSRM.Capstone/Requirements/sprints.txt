# Story : As a user I want to GET all posts

Sub-Task: 	[]	0. Create GetAllPosts stored procedure -UPDATE DB script
		[]	1. Create Post Class with model validations
		[]	2. Create GetAllPosts Repo method using ADO.NET or Dapper -- initial PostRepository Class --create initial connectionString static class -- Create initial RepoTest Class --TEST!!
		[]	3. Create GetAllPosts BLL method with exception handling -- create initial ManagerTests Class-- TEST!!
		[]	4. Create GET method in the "posts API controller" 
		[]	5. Create AJAX GET for home page to display post 
		[]	6. Create home page frame

# Story : As a user or superuser I want to create a post.

Sub-Task: 	[]	0. Create InsertPost stored procedure -UPDATE DB script
		[]	1. Create CreatePost Repo method using ADO.NET  --TEST!!
		[]	2. Create CreatePost BLL method with exception handling  --TEST!!		
		[]	3. Create POST method in home controller
		[]	4. Create The Modal Partial View for tinyMCE
		[]	5. Create Partial View for tinyMCE that is on the homepage
		[]	6. Create Post API Controller and POST method
		[]	7. Create AJAX POST for tinyMCE modal

# Story : As a user I want to Get a single post

Sub-Task:	[]	0. Create GetSinglePost stored procedure --UPDATE DB Script
		[]	1. Create GetSinglePost Repo method using ADO.NET or Dapper in PostRepository Class -- TEST!!
		[]	2. Create GetSinglePost BLL method with exception handling -- TEST!!
		[]	3. Create GET(int id) method in the "posts API controller"
		[]	4. create AJAX GET to get individual post
		[]	5. Create partial modal view to display the individual post


		
# Story : As a superuser I want to delete a post.
		
Sub-Task:	[]	0. Create DeletePost stored procedure -UPDATE DB script
		[]	1. Create DeletePost Repo method using Dapper
		[]	2. Create DeletePost BLL method with exception handling
		[]	3. Create DELETE in POST API CONTROLLER
		[]	4. Create AJAX DELETE for post

#Story : As a visitor I want to request more information

Sub-Task	[]	0. Create InsertInquiry stored procedure -UPDATE DB script
		[]	1. Create 
