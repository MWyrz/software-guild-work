USE Capstone
GO

CREATE PROCEDURE GetPageHeaders

AS

SELECT p.StaticPageID, p.StaticPageTitle
FROM Pages p

GO