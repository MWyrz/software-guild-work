USE [Capstone]
GO

CREATE PROCEDURE GetAllStaticPages

AS

SELECT p.StaticPageID, p.StaticPageTitle
FROM Pages p

GO