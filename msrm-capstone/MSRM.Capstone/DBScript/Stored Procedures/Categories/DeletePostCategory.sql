USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[DeletePostCategory]    Script Date: 3/22/2016 1:53:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[DeletePostCategory](

	@PostId int,
	@CategoryId int

)AS

DELETE FROM PostCategories
WHERE PostID = @PostId And CategoryID = @CategoryId




GO

