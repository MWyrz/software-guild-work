﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace SGCorp.Models.Models
{
    public class Applicant
    {
        public int? IdNumber { get; set; }
        [Required(ErrorMessage = "A Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "A contact number is required")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Enter a valid email address")]
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "Tell us more about your work experiences")]
        public string WorkExperience { get; set; }
        [Required(ErrorMessage = "We require some sort of education")]
        public string Education { get; set; }
        [Required(ErrorMessage = "Who will vouch for you?")]
        public string References { get; set; }
        [Required(ErrorMessage = "Tell us a story")]
        public string AdditionalInfo { get; set; }
    }
}
