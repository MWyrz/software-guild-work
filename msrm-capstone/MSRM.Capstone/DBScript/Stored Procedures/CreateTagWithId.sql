USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[AddTag]    Script Date: 3/15/2016 11:15:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[AddTag]
(
	@TagID int output,
	@TagName nvarchar(50)
)AS


Insert Into Tags
Values (@TagName)
SET @TagID = SCOPE_IDENTITY();


GO

