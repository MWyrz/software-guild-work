﻿using System.Collections.Generic;
using System.Linq;
using BaseballLeague.CONTRACTS.Repository;
using BaseballLeague.DATA;
using BaseballLeague.MODELS;
using NUnit.Framework;

namespace BaseballLeague.TESTS.Repository
{
    [TestFixture]
    public class PlayerRepoTests
    {
        private IPlayerRepository _playerRepo;
        private Player _testPlayer;

        [SetUp]
        public void TestSetup()
        {
            _playerRepo = RepositoryFactory.GetPlayerRepository();
            _testPlayer = new Player();
        }

        [TearDown]
        public void TestTearDown()
        {
            Utilities.RebuildTestDb();
        }

        [Test]
        public void CanLoadAll()
        {
            List<Player> actual = _playerRepo.LoadAll();

            Assert.AreEqual(72, actual.Count);
        }

        [Test]
        public void CanLoad()
        {
            Player actual = _playerRepo.Load(2);

            Assert.AreEqual(2 , actual.PlayerId);
        }

        [Test]
        public void CanAddPlayer()
        {
            _testPlayer.TeamId = 2;
            _testPlayer.FirstName = "Bob";
            _testPlayer.LastName = "Smith";
            _testPlayer.JerseyNumber = 77;

            var actual = _playerRepo.Add(_testPlayer);
            
            Assert.AreEqual(73, _playerRepo.LoadAll().Count);
            Assert.AreEqual(73, actual.PlayerId);
        }

        [Test]
        public void CanEditPlayer()
        {
            
        }

        [Test]
        public void CanRemovePlayer()
        {
            _playerRepo.Remove(10);

            Assert.AreEqual(71, _playerRepo.LoadAll().Count);
            Assert.IsFalse(_playerRepo.LoadAll().Select(p => p.PlayerId).Contains(10));
        }
    }
}