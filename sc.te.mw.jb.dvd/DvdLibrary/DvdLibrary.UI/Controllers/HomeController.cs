﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DvdLibrary.DATA.Repository;
using DvdLibrary.MODELS;

namespace DvdLibrary.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            DvdRepository repo = new DvdRepository();
            var d = repo.GetAllDvds();

            return View(d);
        }

        public ActionResult SingleDvdInfo(int dvdId)
        {
            DvdRepository repo = new DvdRepository();
            var sd = repo.GetDvdInfo(dvdId);

            return View(sd);
        }

        public ActionResult AddDvd()
        {
            DvdRepository repo = new DvdRepository();
            ViewData["Actors"] = repo.GetAllActors();
            ViewData["Directors"] = repo.GetAllDirectors();
            ViewData["Studios"] = repo.GetAllStudios();


            return View(new Dvd());
        }

        [HttpPost]
        public ActionResult AddDvd(Dvd dvd, int ActorId, int DirectorId, int StudioId)
        {
            DvdRepository repo = new DvdRepository();
            if (ModelState.IsValid)
            {
                dvd.Actor.ActorId = ActorId;
                dvd.Director.DirectorId = DirectorId;
                dvd.Studio.StudioId = StudioId;
                repo.AddDvd(dvd);
                return RedirectToAction("Index");
            }
            ViewData["Actors"] = repo.GetAllActors();
            ViewData["Directors"] = repo.GetAllDirectors();
            ViewData["Studios"] = repo.GetAllStudios();

          return View(dvd);
          }
    

    public ActionResult AddActor()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddActor(Actor actor)
        {
            if (ModelState.IsValid)
            {
                DvdRepository repo = new DvdRepository();
                repo.AddActor(actor);
                return RedirectToAction("AddDvd");
            }
            return View(actor);
        }

        public ActionResult AddDirector()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddDirector(Director director)
        {
            if (ModelState.IsValid)
            {
                DvdRepository repo = new DvdRepository();
            repo.AddDirector(director);
            return RedirectToAction("AddDvd");
            }
            return View(director);

        }

        public ActionResult AddStudio()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddStudio(Studio studio)
        {
            if (ModelState.IsValid)
            {
                 DvdRepository repo = new DvdRepository();
            repo.AddStudio(studio);
            return RedirectToAction("AddDvd");
            }
            return View(studio);
        }

        public ActionResult SearchDvd(string search)
        {
            DvdRepository repo = new DvdRepository();
            var searchList = repo.SearchDvdLibrary(search);
            return View(searchList);
        }

        [HttpPost]
        public ActionResult RemoveDvd(int dvdId)
        {
            DvdRepository repo = new DvdRepository();
            repo.RemoveDvd(dvdId);
            return RedirectToAction("Index");
        }

        public ActionResult BorrowerInfo(int dvdId)
        {
            DvdRepository repo = new DvdRepository();
            var borrowerList = repo.GetBorrowerInfo(dvdId);
            return View(borrowerList);
        }
    }
}