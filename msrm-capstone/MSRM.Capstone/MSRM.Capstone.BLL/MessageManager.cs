﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSRM.Capstone.Data;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.BLL
{
    public class MessageManager
    {
        private MessageRepository _repo = new MessageRepository();

        public MessageManager()
        {
            _repo = new MessageRepository();
        }

        public Response<Message> CreateMessage(Message newMsg)
        {
            var response = new Response<Message>();
            try
            {
                int newMsgId = _repo.CreateMessage(newMsg);
                newMsg.MessageId = newMsgId;
                response.Success = true;
                response.Message = "Message Created.";
                response.Data = newMsg;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<List<Message>> GetAllMessages()
        {
            var response = new Response<List<Message>>();

            try
            {
                List<Message> msgList = _repo._messageList;
                if (msgList.Count != 0)
                {
                    response.Success = true;
                    response.Data = msgList;
                }
                else
                {
                    response.Success = false;
                    response.Message = "There are No Messages";
                    response.Data = null;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<Message> GetMessageFromId(int msgId)
        {
            var response = new Response<Message>();

            try
            {
                Message msg = _repo.GetMessageFromId(msgId);
                if (msg != null)
                {
                    response.Success = true;
                    response.Data = msg;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Unable to Retrieve Message.";
                    response.Data = null;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<Message> DeleteMessage(int msgId)
        {
            var response = new Response<Message>();

            try
            {
                _repo.DeleteMessage(msgId);
                if (_repo._messageList.Any(m => m.MessageId == msgId))
                {
                    response.Success = false;
                    response.Message = "Error. Message not Deleted.";
                }
                else
                {
                    response.Success = true;
                    response.Message = "Message Deleted.";
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<Message> UpdateMessage(Message updateMsg)
        {
            var response = new Response<Message>();

            try
            {
                _repo.UpdateMessage(updateMsg);
                response.Success = true;
                response.Message = "Message Updated.";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }
    }
}
