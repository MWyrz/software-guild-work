﻿using System;

namespace BaseballLeague.MODELS
{
    public class Player
    {
        public int PlayerId { get; set; }
        public int TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int JerseyNumber { get; set; }
        public Position Position { get; set; }
        public DateTime? RookieYear { get; set; }
        public decimal? LastSeasonBatAvg { get; set; }
    }
}