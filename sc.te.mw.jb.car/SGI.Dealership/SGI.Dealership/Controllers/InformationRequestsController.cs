﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SGI.Dealership.Models;

namespace SGI.Dealership.Controllers
{
    public class InformationRequestsController : ApiController
    {
        private SgiDealershipContext db = new SgiDealershipContext();

        // GET: api/InformationRequests
        public IQueryable<InformationRequest> GetInformationRequests()
        {
            return db.InformationRequests;
        }

        // GET: api/InformationRequests/5
        [ResponseType(typeof(InformationRequest))]
        public IHttpActionResult GetInformationRequest(int id)
        {
            InformationRequest informationRequest = db.InformationRequests.Find(id);
            if (informationRequest == null)
            {
                return NotFound();
            }

            return Ok(informationRequest);
        }

        // PUT: api/InformationRequests/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutInformationRequest(int id, InformationRequest informationRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != informationRequest.Id)
            {
                return BadRequest();
            }

            db.Entry(informationRequest).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InformationRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/InformationRequests
        [ResponseType(typeof(InformationRequest))]
        public IHttpActionResult PostInformationRequest(InformationRequest informationRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.InformationRequests.Add(informationRequest);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = informationRequest.Id }, informationRequest);
        }

        // DELETE: api/InformationRequests/5
        [ResponseType(typeof(InformationRequest))]
        public IHttpActionResult DeleteInformationRequest(int id)
        {
            InformationRequest informationRequest = db.InformationRequests.Find(id);
            if (informationRequest == null)
            {
                return NotFound();
            }

            db.InformationRequests.Remove(informationRequest);
            db.SaveChanges();

            return Ok(informationRequest);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InformationRequestExists(int id)
        {
            return db.InformationRequests.Count(e => e.Id == id) > 0;
        }
    }
}