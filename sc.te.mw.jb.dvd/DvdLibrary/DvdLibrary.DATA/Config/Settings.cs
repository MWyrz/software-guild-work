﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DvdLibrary.DATA.Config
{
    public static class Settings 
    {
        public static string ConnectionString { get; private set; }

        static Settings()
        {
            ConnectionString = ConfigurationManager.AppSettings["Mode"] == "Production"
                ? ConfigurationManager.ConnectionStrings["DvdLibrary"].ConnectionString
                : ConfigurationManager.ConnectionStrings["TestDvdLibrary"].ConnectionString;
        }
    }
}
