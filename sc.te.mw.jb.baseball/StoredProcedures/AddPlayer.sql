USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[AddPlayer]    Script Date: 2/23/2016 12:26:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[AddPlayer]
(
	@TeamId int,
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@JerseyNumber int,
	@RookieYear int,
	@LastSeasonBatAvg decimal(4,3)
) AS

DECLARE @InsertedRecord table
(	
	PlayerId int,
	TeamId int,
	FirstName nvarchar(50),
	LastName nvarchar(50),
	JerseyNumber int,
	RookieYear int,
	LastSeasonBatAvg decimal(4,3)
)

INSERT INTO Players (TeamId, FirstName, LastName, JerseyNumber, RookieYear, LastSeasonBatAvg)
	OUTPUT INSERTED.PlayerId, INSERTED.TeamId, INSERTED.FirstName, INSERTED.LastName,
	INSERTED.JerseyNumber, INSERTED.RookieYear, INSERTED.LastSeasonBatAvg
	INTO @InsertedRecord
VALUES (@TeamId, @FirstName, @LastName, @JerseyNumber, @RookieYear, @LastSeasonBatAvg)

SELECT *
FROM @InsertedRecord
GO

