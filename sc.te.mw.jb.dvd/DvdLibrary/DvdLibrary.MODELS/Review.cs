﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DvdLibrary.MODELS
{
    public class Review
    {
        
        public int BorReviewId { get; set; }
        public int DvdId { get; set; }
        public string Rating { get; set; }
        public string Notes { get; set; }
    }
}
