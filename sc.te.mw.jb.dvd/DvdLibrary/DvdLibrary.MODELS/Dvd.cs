﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DvdLibrary.MODELS
{
    public class Dvd
    {
        public int DvdId { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "Please Enter a Lead Actor")]
        public int ActorId { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "Please Enter The Director")]
        public int DirectorId { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "Please Enter The Studio Name")]
        public int StudioId { get; set; }
        [Required(ErrorMessage = "Please Enter a Title")]
        public string Title { get; set; }
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Please Enter a Date")]
        public DateTime ReleaseDate { get; set; }
        [Required(ErrorMessage = "Please Enter a MPAA Rating")]
        public string MPAARating { get; set; }
        public Studio Studio { get; set; }
        public Director Director { get; set; }
        public Actor Actor { get; set; }
        public List<Borrower> Borrowers { get; set;} 
        public List<Review> Reviews { get; set; }

        public Dvd()
        {
            Actor = new Actor();
            Director = new Director();
            Studio = new Studio();
        }
    }
}
