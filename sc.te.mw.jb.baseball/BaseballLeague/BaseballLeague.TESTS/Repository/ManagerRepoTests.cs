﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using BaseballLeague.CONTRACTS.Repository;
using BaseballLeague.DATA;
using BaseballLeague.MODELS;
using NUnit.Framework;

namespace BaseballLeague.TESTS.Repository
{
    [TestFixture]
    public class ManagerRepoTests
    {
        // instantiation
        private IManagerRepository _managerRepository;
        private Manager _testManager;

        [SetUp]
        public void SetUp()
        {
            _managerRepository = RepositoryFactory.GetManagerRepository();
            _testManager = new Manager();
        }

        [TearDown]
        public void TestTearDown()
        {
            
        }

        [Test]
        public void CanLoadAll()
        {
            List<Manager> actual = _managerRepository.LoadAll();

            Assert.AreEqual(8, actual.Count);
        }

        [Test]
        public void CanLoad()
        {
            Manager actual = _managerRepository.Load(5);

            Assert.AreEqual(5, actual.ManagerId);
        }

        [Test]
        public void CanAddManager()
        {
            _testManager.ManagerId = 10;
            _testManager.FirstName = "Owen";
            _testManager.LastName = "Wilson";

            _managerRepository.Add(_testManager);
        }

        [Test]
        public void CanEditManager()
        {
            _testManager.ManagerId = 18;
            _testManager.FirstName = "Luke";
            _testManager.LastName = "Wilson";

            _managerRepository.Edit(10, _testManager);
        }

        [Test]
        public void CanRemoveManager()
        {
            _testManager.ManagerId = 18;
            _testManager.LastName = "Wilson";
        }
    }
}
