﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSRM.Capstone.Data;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.BLL
{
    public class CategoryManager
    {
        public Response<List<Category>> GetAllCategories()
        {
            Response<List<Category>> response = new Response<List<Category>>();
            CategoryRepository repo = new CategoryRepository();
            try
            {
                var categoryList = repo.GetAllCategories();
                if (categoryList != null)
                {
                    response.Success = true;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Unable to load the list of Categories";
                }
                response.Data = categoryList;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public Response<List<Category>> GetPostCategories(int postId)
        {
            Response<List<Category>> response = new Response<List<Category>>();
            CategoryRepository repo = new CategoryRepository();
            try
            {
                var categoryList = repo.GetCategoriesByPostId(postId);
                if (categoryList != null)
                {
                    response.Success = true;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Unable to load post categories.";
                }
                response.Data = categoryList;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public Response<Category> AddCategory(Category category)
        {
            Response<Category> response = new Response<Category>();
            var repo = new CategoryRepository();
            var categoryList = repo.GetAllCategories();
            try
            {
                category.CategoryId = repo.AddCategory(category.CategoryName);
                if (categoryList.Any(c => c.CategoryId != category.CategoryId))
                {
                    if (repo.GetAllCategories().Any(c => c.CategoryName == category.CategoryName))
                    {
                        response.Success = true;
                        response.Message = "You have added a category.";
                        response.Data = category;
                    }
                    else
                    {
                        response.Success = false;
                        response.Message = "Unable to add category. Please Try Again.";
                    }
                }
                else
                {
                    response.Success = false;
                    response.Message = "Category already exists.";
                                        response.Data = category;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<int> DeleteCategory(int categoryId)
        {
            Response<int> response = new Response<int>();
            var repo = new CategoryRepository();

            try
            {
                if (categoryId == 1)
                {
                    response.Data = categoryId;
                    response.Success = false;
                    response.Message = "Cannot delete the category \"Uncategorized\".";
                }
                else
                {

                    repo.DeleteCategory(categoryId);
                    response.Data = categoryId;
                    response.Message = "Category deleted";
                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }
    }
}
