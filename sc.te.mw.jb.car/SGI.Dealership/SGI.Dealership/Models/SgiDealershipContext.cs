using System.ComponentModel.DataAnnotations;

namespace SGI.Dealership.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class SgiDealershipContext : DbContext
    {
        // Your context has been configured to use a 'SgiDealershipContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'SGI.Dealership.Models.SgiDealershipContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'SgiDealershipContext' 
        // connection string in the application configuration file.
        public SgiDealershipContext()
            : base("name=SgiDealershipContext")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<Vehicle> Vehicles { get; set; }
        public virtual DbSet<InformationRequest> InformationRequests { get; set; }
        public virtual DbSet<RequestStatus> RequestStatuses { get; set; }

    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}

    public class InformationRequest
    {
        public int Id { get; set; }
        public int RequestStatusId { get; set; }
        public int VehicleId { get; set; }
        public DateTime DateSubmitted { get; set; }
        public DateTime LastContactDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerBestTimeToContact { get; set; }
        public string CustomerPreferredContactMethod { get; set; }
        public string CustomerTimeFrameToBuy { get; set; }
        public string CustomerAdditionalInformation { get; set; }
    }

    public class RequestStatus
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }

    public class Vehicle
    {
        public int Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string Condition { get; set; }
        public int Mileage { get; set; }
        public decimal Price { get; set; }
        public string AdTitle { get; set; }
        public string Description { get; set; }
    }
}