﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DvdLibrary.MODELS
{
    public class Actor
    {
        public int ActorId { get; set; }
        [Required(ErrorMessage = "Please Enter Actors First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please Enter Actors Last Name")]
        public string LastName { get; set; }
    }
}
