﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using MSRM.Capstone.Data.Settings;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.Data
{
    public class StatusRepository
    {
        public List<Statuses> GetAllStatus()
        {
            List<Statuses> statuses = new List<Statuses>();
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                statuses = cn.Query<Statuses>("GetStatuses", commandType: CommandType.StoredProcedure).ToList();
            }

            return statuses;
        }
    }
}
