﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseballLeague.MODELS;

namespace BaseballLeague.CONTRACTS.Repository
{
    public interface ITeamRepository
    {
        List<Team> LoadAll();
        Team Load(int teamId);
        Team Add(Team team);
        Team Edit(int teamId, Team team);
        int Remove(int teamId);
    }
}
