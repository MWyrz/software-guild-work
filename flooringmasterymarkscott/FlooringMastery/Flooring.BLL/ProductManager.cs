﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data;
using Flooring.Models;

namespace Flooring.BLL
{
    public class ProductManager
    {
        private bool production;
        public ProductManager(bool config)
        {
            production = config;
        }
        public ProductManager(string config)
        {
            if (config == "true")
            {
                production = true;
            }
            else
            {
                production = false;
            }
        }
        public decimal[] GetProductPricing(string pType)
        {
            IDataSelector data;
            if (production)
            {
                data = new ProdDB("", "products");
            }
            else
            {
                data = new MockDB("", "products");
            }
            string[] lines = data.GetData().ToArray();
            for (int i = 0; i < lines.Length; i++)
            {
                string[] columns = lines[i].Split(',');
                if (pType == columns[0])
                {
                    return new decimal[] { decimal.Parse(columns[1]), decimal.Parse(columns[2]) };
                }
            }
            return new decimal[0];
        }

        public bool CheckProduct(string product)
        {
            if (GetProductPricing(product).Length == 0)
            {
                return false;
            }
            return true;
        }
    }
}
