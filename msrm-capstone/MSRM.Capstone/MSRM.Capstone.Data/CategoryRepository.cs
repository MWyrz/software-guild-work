﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MSRM.Capstone.Data.Settings;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.Data
{
    public class CategoryRepository
    {
        public List<Category> GetAllCategories()
        {
            var categoryList = new List<Category>();
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                categoryList = cn.Query<Category>("SELECT * FROM Categories").ToList();
            }
            return categoryList;
        }

        public List<Category> GetCategoriesByPostId(int postId)
        {
            var categoryList = new List<Category>();
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "GetPostCategories";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@PostId", postId);

                cn.Open();
                
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Category category = new Category();
                        category.CategoryId = (int) dr["CategoryID"];
                        category.CategoryName = dr["CategoryName"].ToString();
                        categoryList.Add(category);
                    }
                }
            }
            return categoryList;
        }

        public int AddCategory(string categoryName)
        {
            int categoryId = -1;
            var categoryList = GetAllCategories();
            if (categoryList.Any(c => c.CategoryName == categoryName))
            {
                categoryId = categoryList.First(c => c.CategoryName == categoryName).CategoryId;
                return categoryId;
            }
            else
            {
                using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
                {
                    var c = new DynamicParameters();
                    c.Add("CategoryName", categoryName);
                    c.Add("CategoryId",DbType.Int32,direction:ParameterDirection.Output);

                    cn.Execute("AddCategory", c, commandType: CommandType.StoredProcedure);
                    categoryId = c.Get<int>("CategoryId");
                    return categoryId;
                }
            }
        }

        public List<int> AddCategoriesFromPost(List<string> categories, int postId)
        {
            List<int> listOfCategoryIds = new List<int>();
            foreach (var categoryName in categories)
            {
                listOfCategoryIds.Add(AddCategory(categoryName));
            }
            if (listOfCategoryIds.Count != 0)
            {
                using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
                {
                    foreach (var id in listOfCategoryIds)
                    {
                        var c = new DynamicParameters();
                        c.Add("PostId", postId);
                        c.Add("CategoryId", id);

                        cn.Execute("AddPostCategories", c, commandType: CommandType.StoredProcedure);
                    }
                }
            }
            return listOfCategoryIds;
        }

        public void DeleteCategory(int categoryId)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var c = new DynamicParameters();
                c.Add("CategoryId", categoryId);

                cn.Execute("DeleteCategory", c, commandType: CommandType.StoredProcedure);
                cn.Execute("AddToUncategorized", commandType: CommandType.StoredProcedure);
            }
        }
    }
}
