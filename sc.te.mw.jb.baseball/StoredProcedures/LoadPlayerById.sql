USE [TestBaseballLeague]
GO

/****** Object:  StoredProcedure [dbo].[LoadPlayerById]    Script Date: 2/23/2016 12:26:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LoadPlayerById]
(
	@PlayerId int
) AS

SELECT *
FROM Players p
WHERE p.PlayerId = @PlayerId
GO

