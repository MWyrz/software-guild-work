﻿using System;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL;

namespace Flooring.UI
{
    public class DisplayOrderWorkflow
    {
        public void Execute()
        {
            string config = ConfigurationSettings.AppSettings.Get("production");
            string date = UserPrompts.GetDateFromUser();
            OrderManager manager = new OrderManager(config);

            var response = manager.DisplayOrders(date);
            if (response.Success)
            {
                foreach (var order in response.Data)
                {
                    manager.DisplayOrder(order);
                }
            }
            else
            {
                Console.WriteLine(response.Message);
            }
            UserPrompts.PressAnyKeyToContinue();
        }
    }
}
