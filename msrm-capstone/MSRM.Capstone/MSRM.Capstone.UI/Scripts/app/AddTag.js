﻿var addATagUri = '/api/Tags/';

$(document).ready(function () {
    $(document).on("click", "#btnAddTag", function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $('#AddATagModal').modal('show');
    });
    $(document).on("click", "#SubmitTag", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var tag = {};

        tag.Name = $('#Name').val();
        $.post(addATagUri + 'post/', tag).success(function (data) {
        })
            .error(function (e, status) {
            }).done(function () {
                $('#AddATagModal').modal('hide');
                location.reload();
            });
    });

});
