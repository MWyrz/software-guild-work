USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[GetTagIdFromTagName]    Script Date: 3/14/2016 9:24:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[GetTagIdFromTagName]
(
	@TagName nvarchar(50)
)AS

Select t.TagID
From Tags t
WHERE t.Tag = @TagName


GO

