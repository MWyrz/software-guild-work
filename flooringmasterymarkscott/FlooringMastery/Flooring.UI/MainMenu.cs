﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI
{
    public class MainMenu
    {
        public void Execute()
        {
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("* Flooring Program ");
            Console.WriteLine("*");
            Console.WriteLine("* 1. Display Orders ");
            Console.WriteLine("* 2. Add an Order ");
            Console.WriteLine("* 3. Edit an Order ");
            Console.WriteLine("* 4. Remove an Order ");
            Console.WriteLine("* 5. Quit ");
            Console.WriteLine("*");
            Console.WriteLine("*******************************************************************************");
            Console.Write("Enter a number to select an option: ");
            string input = Console.ReadLine();
            ProcessChoice(input);
        }

        public void ProcessChoice(string choice)
        {
            switch (choice)
            {
                case "1":
                    DisplayOrderWorkflow display = new DisplayOrderWorkflow();
                    display.Execute();
                    break;
                case "2":
                    AddOrderWorkflow addOrder = new AddOrderWorkflow();
                    addOrder.Execute();
                    break;
                case "3":
                    EditOrderWorkflow editOrder = new EditOrderWorkflow();
                    editOrder.Execute();
                    break;
                case "4":
                    RemoveOrderWorkflow remove = new RemoveOrderWorkflow();
                    remove.Execute();
                    break;
                case "5":
                    Environment.Exit(0);
                    break;
            }
        }
    }
}
