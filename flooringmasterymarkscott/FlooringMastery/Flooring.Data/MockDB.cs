﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Models;

namespace Flooring.Data
{
    public class MockDB : IDataSelector
    {
        private string _fileName;
        public MockDB(string file, string toRead)
        {
            switch (toRead)
            {
                case "orders":
                    DateTime date = new DateTime();

                    date = DateTime.Parse(file);
                    string dateString;
                    if (date.Month >= 10)
                    {
                        dateString = date.Month.ToString();
                    }
                    else
                    {
                        dateString = "0" + date.Month;
                    }

                    if (date.Day >= 10)
                    {
                        dateString += date.Day.ToString();
                    }
                    else
                    {
                        dateString += "0" + date.Day;
                    }

                    dateString += date.ToString("yyy");
                    _fileName = @"Testing/OrderFiles/Orders_" + dateString + ".txt";
                    break;
                case "products":
                    _fileName = @"Testing/DataFiles/Products.txt";
                    break;
                case "taxes":
                    _fileName = @"Testing/DataFiles/Taxes.txt";
                    break;
                case "error":
                    _fileName = @"Testing/Errors.txt";
                    break;
            }
        }
        public List<string> GetData()
        {

            List<string> output = new List<string>();

            if (File.Exists(_fileName))
            {
                var reader = File.ReadAllLines(_fileName);

                for (int i = 1; i < reader.Length; i++)
                {
                    output.Add(reader[i]);
                }
            }
            return output;
        }

        public List<string> GetErrorData()
        {
            List<string> output = new List<string>();

            if (File.Exists(_fileName))
            {
                var reader = File.ReadAllLines(_fileName);

                for (int i = 0; i < reader.Length; i++)
                {
                    output.Add(reader[i]);
                }
            }
            return output;
        } 

        public void AddData(List<Order> orders)
        {
            File.Delete(_fileName);

            using (var writer = File.CreateText(_fileName))
            {
                writer.WriteLine("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total");

                foreach (var order in orders)
                {
                    writer.WriteLine("{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}",
                        order.OrderNumber,
                        order.CustomerName.Replace(",",",,"),
                        order.State,
                        order.TaxRate,
                        order.ProductType,
                        order.Area,
                        order.CostPerSquareFoot,
                        order.LaborCostPerSquareFoot,
                        order.MaterialCost,
                        order.LaborCost,
                        order.Tax,
                        order.Total);
                }
            }
        }

        public void WriteErrors(string newError)
        {
            List<string> errorList = GetErrorData();
            errorList.Add(newError);

            using (var writer = File.CreateText(_fileName))
            {
                foreach (var error in errorList)
                {
                    writer.WriteLine(error);
                }
            }
        }
    }
}
