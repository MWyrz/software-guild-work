﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL;
using Flooring.Data;
using Flooring.Models;
using NUnit.Framework;

namespace Flooring.Tests
{
    [TestFixture]

    public class ManagerTests
    {
        [TestCase("OH", true)]
        [TestCase("DC", false)]
        public void CheckStateTaxTest(string state, bool expected)
        {
            StateTaxManager taxManager = new StateTaxManager(false);

            bool result = taxManager.CheckStateTax(state);

            Assert.AreEqual(expected, result);
        }

        [TestCase("OH", 6.25)]
        [TestCase("PA", 6.75)]
        [TestCase("DC", -1.00)]
        public void GetTaxRateTest(string state, decimal expected)
        {
            StateTaxManager taxMgr = new StateTaxManager(false);

            decimal result = taxMgr.GetTaxRate(state);

            Assert.AreEqual(expected, result);
        }

        [TestCase("SandStone", false)]
        [TestCase("Tile", true)]
        [TestCase("Laminate", true)]
        public void CheckProductTest(string product, bool expected)
        {
            ProductManager prodMgr = new ProductManager(false);

            bool result = prodMgr.CheckProduct(product);

            Assert.AreEqual(expected, result);
        }

        [TestCase("SandStone", new double[] {})]
        [TestCase("Tile", new [] {3.50, 4.15})]
        [TestCase("Laminate", new [] { 1.75, 2.10 })]
        public void GetProductPricingTest(string pType, double[] numbers)
        {
            ProductManager productManager = new ProductManager(false);
            decimal[] expected = new decimal[numbers.Length];
            for(int i = 0; i<numbers.Length;i++)
            {
                expected[i] = (decimal) numbers[i];
            }

            decimal[] result = productManager.GetProductPricing(pType);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetOrdersTest()
        {
            OrderManager orderManager = new OrderManager("false");
            List<string> list = new List<string>();

            list.Add("4 \",\"Ahhnold Schwarzengruuuben\",\"PA\",\"6.75\",\"Tile\",\"100000\",\"3.50\",\"4.15\",\"350000.00\",\"415000.00\",\"51637.500000\",\"816637.500000");
            List<Order> orders = orderManager.GetOrders(list);
            foreach (var order in orders)
            {
                Assert.AreEqual(order.OrderNumber, 4);
                Assert.AreEqual(order.CustomerName, "Ahhnold Schwarzengruuuben");
                Assert.AreEqual(order.State, "PA");
                Assert.AreEqual(order.TaxRate, 6.75);
                Assert.AreEqual(order.ProductType, "Tile");
                Assert.AreEqual(order.Area, 100000);
                Assert.AreEqual(order.CostPerSquareFoot, 3.50);
                Assert.AreEqual(order.LaborCostPerSquareFoot, 4.15);
                Assert.AreEqual(order.MaterialCost, 350000.00);
                Assert.AreEqual(order.LaborCost, 415000.00);
                Assert.AreEqual(order.Tax, 51637.500000);
                Assert.AreEqual(order.Total, 816637.500000);
            }
        }

        [Test]
        public void RemoveOrderTest()
        {
            OrderManager ordMgr = new OrderManager("false");

            string date = "01/01/2015";
            int number = 3;

            var response = ordMgr.DisplayOrders(date);
            Order orderToRemove = new Order();
            foreach (var order in response.Data)
            {
                if (order.OrderNumber == number)
                {
                    orderToRemove = order;
                }
            }
            ordMgr.RemoveOrder(date, orderToRemove);

            var result = ordMgr.DisplayOrders(date);

            foreach (var order in result.Data)
            {
                Assert.IsTrue(order.OrderNumber != number);
            }
        }

        [Test]
        public void AddOrderTest()
        {
            MockDB mockDb = new MockDB("01/01/2015", "orders");
            OrderManager ordMan = new OrderManager("false");
            Order addOrder = new Order();

            string date = "01/01/2015";
            addOrder.CustomerName = "Florence Henderson";
            addOrder.State = "PA";
            addOrder.Area = 1500;
            addOrder.ProductType = "Laminate";
            addOrder = ordMan.GenerateOrder(date, addOrder).Data;

            ordMan.AddOrder(addOrder, date);

            List<Order> newOrderList = ordMan.GetOrders(mockDb.GetData());

            bool newOrder = false;

            foreach (var order in newOrderList)
            {
                if (order.OrderNumber == addOrder.OrderNumber)
                {
                    newOrder = true;
                }
            }
            Assert.IsTrue(newOrder);
        }
    }
}
