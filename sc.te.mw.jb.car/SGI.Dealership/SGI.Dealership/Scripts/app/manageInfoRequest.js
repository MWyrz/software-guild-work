﻿var requests = [];

$(document).ready(function () {
    $("#btnLoadInfoRequests").click(function () {
        $("#manageRequestsModal").modal("show");

        loadAllInfoRequests();
    });
});

function loadAllInfoRequests() {
    $.getJSON(infoRequestUri)
        .done(function(data) {
            requests = data;
            loadRequest(requests[0]);
        })
        .fail(function(jqXhr, status, err) {
            window.fillError(err);
        });
}

function loadRequest(req) {
    $("#Id").val(req.Id);
    $("#DateSubmitted").val(moment(req.DateSubmitted).format("MMM D YYYY"));

    if (req.DateSubmitted === req.LastContactDate) {
        $("#LastContactDate").val("n / a");
    } else {
        $("#LastContactDate").val(moment(req.LastContactDate).format("MMM D YYYY"));

    }

    $("#CustomerName").val(req.CustomerName);
    $("#CustomerPhone").val(req.CustomerPhone);
    $("#CustomerEmail").val(req.CustomerEmail);
    $("#CustomerBestTimeToContact").val(req.CustomerBestTimeToContact);
    $("#CustomerPreferredContactMethod").val(req.CustomerPreferredContactMethod);
    $("#CustomerTimeFrameToBuy").val(req.CustomerTimeFrameToBuy);
    $("#CustomerAdditionalInformation").val(req.CustomerAdditionalInformation);


    $.getJSON(vehicleUri + req.VehicleId)
        .done(function(data) {
            $("#VehicleId").val(data.Year + " " +  data.Make + " " + data.Model);
        })
        .fail(function(jqXhr, status, err) {
            $("<div class=\"col-xs-offset-1 col-xs-10 alert alert-danger\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a>" + err + "</div>").appendTo("#errorMsg");
        });
    $.getJSON(reqStatusUri + req.RequestStatusId)
        .done(function(data) {
            $("#RequestStatusId").val(data.Status);
        })
    .fail(function (jqXhr, status, err) {
            window.fillError(err);
        });
}