﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DvdLibrary.DATA.Config;
using DvdLibrary.MODELS;

namespace DvdLibrary.DATA.Repository
{
    public class DvdRepository
    {

        public List<Dvd> GetAllDvds()
        {
            var dvdList = new List<Dvd>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                dvdList = cn.Query<Dvd>("SELECT * FROM Dvd").ToList();
            }
            return dvdList;
        }

        public Dvd GetDvdInfo(int dvdId)
        {
            var oneDvd = new Dvd();

            oneDvd.Studio = new Studio();
            oneDvd.Actor = new Actor();
            oneDvd.Director = new Director();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {

                var cmd = new SqlCommand();
                cmd.CommandText = "GetDvdInfo";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@DvdId", dvdId);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        oneDvd.DvdId = dvdId;
                        oneDvd.Title = dr["Title"].ToString();
                        oneDvd.MPAARating = dr["MPAARating"].ToString();
                        oneDvd.ReleaseDate = (DateTime)dr["ReleaseDate"];
                        oneDvd.Studio.StudioId = (int)dr["StudioId"];
                        oneDvd.Studio.StudioName = dr["StudioName"].ToString();
                        oneDvd.Actor.ActorId = (int)dr["ActorId"];
                        oneDvd.Actor.FirstName = dr["ActorFirstName"].ToString();
                        oneDvd.Actor.LastName = dr["ActorLastName"].ToString();
                        oneDvd.Director.DirectorId = (int)dr["DirectorId"];
                        oneDvd.Director.FirstName = dr["DirectorFirstName"].ToString();
                        oneDvd.Director.LastName = dr["DirectorLastName"].ToString();
                    }
                }
            }
            oneDvd.Reviews =  GetDvdReviews(dvdId);
            return oneDvd;
        }

        public List<Review> GetDvdReviews(int dvdId)
        {
            var reviewList = new List<Review>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var r = new DynamicParameters();
                r.Add("DvdId", dvdId);
                reviewList = cn.Query<Review>("SELECT * FROM BorrowerReview WHERE DvdId = @DvdId", r).ToList();

            }
            return reviewList;
        }

        public Dvd AddDvd(Dvd newDvd)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var d = new DynamicParameters();
                d.Add("StudioId", newDvd.Studio.StudioId);
                d.Add("Title", newDvd.Title);
                d.Add("ReleaseDate", newDvd.ReleaseDate);
                d.Add("MPAARating", newDvd.MPAARating);
                d.Add("ActorId", newDvd.Actor.ActorId);
                d.Add("DirectorId", newDvd.Director.DirectorId);
                d.Add("DvdId", DbType.Int32, direction: ParameterDirection.Output);

                cn.Execute("AddDvd", d, commandType: CommandType.StoredProcedure);

                newDvd.DvdId = d.Get<int>("DvdId");
                return newDvd;
            }
        }

        public void RemoveDvd(int dvdId)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var r = new DynamicParameters();
                r.Add("DvdId", dvdId);
                cn.Execute("RemoveDvd", r, commandType: CommandType.StoredProcedure);
            }
        }

        public List<Borrower> GetBorrowerInfo(int dvdId)
        {
            var borrowerList = new List<Borrower>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var r = new DynamicParameters();
                r.Add("DvdId", dvdId);
                borrowerList = cn.Query<Borrower>("SELECT b.BorrowerId, b.FirstName + ' ' + b.LastName AS BorrowerName, db.DateBorrowed, db.DateReturned " +
                                                    "FROM DvdBorrowersInfo db " +
                                                    "INNER JOIN Borrowers b " +
                                                    "ON db.BorrowerId = b.BorrowerId WHERE DvdId = @DvdId", r).ToList();
                }
            return borrowerList;
        }

        public List<Actor> GetAllActors()
        {
            var actorList = new List<Actor>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                actorList = cn.Query<Actor>("SELECT * FROM Actors").ToList();
            }
            return actorList;
        }

        public List<Director> GetAllDirectors()
        {
            var directorList = new List<Director>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                directorList = cn.Query<Director>("SELECT * FROM Directors").ToList();
            }
            return directorList;
        }

        public List<Studio> GetAllStudios()
        {
            var studioList = new List<Studio>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                studioList = cn.Query<Studio>("SELECT * FROM Studio").ToList();
            }
            return studioList;
        }

        public void AddActor(Actor actor)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var d = new DynamicParameters();
                d.Add("ActorFirstName", actor.FirstName);
                d.Add("ActorLastName", actor.LastName);

                cn.Execute("INSERT INTO Actors(FirstName, LastName) VALUES(@FirstName, @LastName)", d);
            }
        }

        public void AddDirector(Director director)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var d = new DynamicParameters();
                d.Add("DirectorFirstName", director.FirstName);
                d.Add("DirectorLastName", director.LastName);

                cn.Execute("INSERT INTO Directors(FirstName, LastName) VALUES(@FirstName, @LastName)", d);
            }
        }

        public void AddStudio(Studio studio)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var d = new DynamicParameters();
                d.Add("StudioName", studio.StudioName);

                cn.Execute("INSERT INTO Studio(StudioName) VALUES(@StudioName)", d);
            }
        }

        public List<Dvd> SearchDvdLibrary(string searchName)
        {
            var fullList = GetAllDvds();
            return fullList.Where(dvd => dvd.Title.ToUpper().Contains(searchName.ToUpper())).ToList();
        }
    }
}
