﻿$(document).ready(function() {
    var mn = $(".main-nav");
    var bar = $(".container-fluid");

    $(window).scroll(function() {
        if($(this).scrollTop() > 270) {
            mn.addClass("main-nav-scrolled");
            bar.addClass("main-nav-body-scrolled");
        } else {
            mn.removeClass("main-nav-scrolled");
            bar.removeClass("main-nav-body-scrolled");
        }
    });
});