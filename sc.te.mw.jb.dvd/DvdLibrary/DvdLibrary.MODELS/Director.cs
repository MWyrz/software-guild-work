﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DvdLibrary.MODELS
{
    public class Director
    {
        public int DirectorId { get; set; }
        [Required(ErrorMessage = "Please Enter Directors First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please Enter Directors Last Name")]
        public string LastName { get; set; }
    }
}
