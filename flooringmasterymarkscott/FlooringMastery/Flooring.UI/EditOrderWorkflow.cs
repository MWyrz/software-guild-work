﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL;
using Flooring.Models;

namespace Flooring.UI
{
    public class EditOrderWorkflow
    {
        public void Execute()
        {
            string config = ConfigurationSettings.AppSettings.Get("production");
            string date = UserPrompts.GetDateFromUser();
            bool properState = false;
            bool properType = false;
            OrderManager manager = new OrderManager(config);
            StateTaxManager tax = new StateTaxManager(config);
            ProductManager product = new ProductManager(config);
            Response<Order> result = new Response<Order>();
            Order editedOrder = new Order();

            var response = manager.DisplayOrders(date);
            if (response.Success)
            {
                int number = UserPrompts.GetOrderNumberFromUser();
                foreach (var order in response.Data)
                {
                    if (order.OrderNumber == number)
                    {
                        Console.WriteLine("The current Customer Name is: {0}", order.CustomerName);
                        if (!UserPrompts.GetUserConfirmation())
                        {
                            editedOrder.CustomerName = UserPrompts.GetCustomerNameFromUser();
                        }
                        else
                        {
                            editedOrder.CustomerName = order.CustomerName;
                        }

                        Console.WriteLine("\nThe current State is: {0}", order.State);
                        if (!UserPrompts.GetUserConfirmation())
                        {
                            while (!properState)
                            {
                                editedOrder.State = UserPrompts.GetStateFromUser();
                                properState = tax.CheckStateTax(editedOrder.State);
                                if (!properState)
                                {
                                    Console.WriteLine("\nWe cannot accept orders from " + editedOrder.State + ".\n");
                                    manager.AddError(DateTime.Now + ": User entered an invalid state(" + order.State + ").");
                                }
                            }
                        }
                        else
                        {
                            editedOrder.State = order.State;
                        }
                        Console.WriteLine("\nThe current Product Type is: {0}", order.ProductType);
                        if (!UserPrompts.GetUserConfirmation())
                        {
                            while (!properType)
                            {
                                editedOrder.ProductType = UserPrompts.GetProductTypeFromUser();
                                properType = product.CheckProduct(editedOrder.ProductType);
                                if (!properType)
                                {
                                    Console.WriteLine("\nThat product is not available.\n");
                                    manager.AddError(DateTime.Now + ": User entered an invalid product type(" + order.ProductType + ").");
                                }
                            }
                        }
                        else
                        {
                            editedOrder.ProductType = order.ProductType;
                        }

                        Console.WriteLine("\nThe current Area is: {0}", order.Area);
                        if (!UserPrompts.GetUserConfirmation())
                        {
                            editedOrder.Area = UserPrompts.GetAreaFromUser();
                        }
                        else
                        {
                            editedOrder.Area = order.Area;
                        }

                        result = manager.GenerateOrder(date, editedOrder);
                        
                            result.Success = true;

                            editedOrder.OrderNumber = order.OrderNumber;
                            manager.DisplayOrder(editedOrder);
                        if (UserPrompts.GetUserConfirmation())
                        {

                            order.CustomerName = editedOrder.CustomerName;
                            order.State = editedOrder.State;
                            order.ProductType = editedOrder.ProductType;
                            order.Area = editedOrder.Area;
                            order.TaxRate = editedOrder.TaxRate;
                            order.CostPerSquareFoot = editedOrder.CostPerSquareFoot;
                            order.LaborCostPerSquareFoot = editedOrder.LaborCostPerSquareFoot;
                        }
                        else
                        {
                            Console.WriteLine("\nChanges discarded.\n");
                            UserPrompts.PressAnyKeyToContinue();
                        }

                    }
                }
                if (result.Success)
                {
                    manager.EditOrder(date, response.Data);
                }
                else
                {
                    Console.WriteLine("There is no order with number {0} on {1}.",number,date);
                    manager.AddError(DateTime.Now + ": User entered an invalid order number(" + number + ") for date(" + date + ").");
                    UserPrompts.PressAnyKeyToContinue();
                }
            }
            else
            {
                Console.WriteLine(response.Message);
                UserPrompts.PressAnyKeyToContinue();
            }
        }
    }
}
