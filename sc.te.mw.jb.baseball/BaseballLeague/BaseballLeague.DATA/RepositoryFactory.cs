﻿using BaseballLeague.CONTRACTS.Repository;
using BaseballLeague.DATA.Repository;

namespace BaseballLeague.DATA
{
    public static class RepositoryFactory
    {
        public static IPlayerRepository GetPlayerRepository()
        {
            return new PlayerRepository();
        }

        public static IManagerRepository GetManagerRepository()
        {
            return new ManagerRepository();
        }
    }
}