USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[AddTag]    Script Date: 3/10/2016 10:24:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[AddTag]
(
	@TagName nvarchar(50)
)AS


Insert Into Tags
Values (@TagName)


GO

