USE [TestDvdLibrary]
GO
/****** Object:  StoredProcedure [dbo].[GetDvdInfo]    Script Date: 2/26/2016 11:12:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetDvdInfo]
(
   @DvdId int
) AS

SELECT d.Title, d.ReleaseDate, d.MPAARating,s.StudioId, s.StudioName,
a.ActorId, a.FirstName AS ActorFirstName, a.LastName AS ActorLastName ,di.DirectorId, di.FirstName AS DirectorFirstName, di.LastName AS DirectorLastName
FROM Dvd d
INNER JOIN DvdActors da
ON d.DvdId = da.DvdId
INNER JOIN Actors a
ON da.ActorId = a.ActorId
INNER JOIN DvdDirectors dd
ON d.DvdId = dd.DvdId
INNER JOIN Directors di
ON dd.DirectorId = di.DirectorId
INNER JOIN Studio s
ON d.StudioId = s.StudioId
WHERE d.DvdId = 1