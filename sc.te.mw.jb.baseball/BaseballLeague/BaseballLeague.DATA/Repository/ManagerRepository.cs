﻿using System.Collections.Generic;
using System.Data.SqlClient;
using BaseballLeague.CONTRACTS.Repository;
using BaseballLeague.DATA.Config;
using BaseballLeague.MODELS;

namespace BaseballLeague.DATA.Repository
{
    public class ManagerRepository : IManagerRepository
    {
        private readonly string _cnStr;

        public ManagerRepository()
        {
            _cnStr = Settings.ConnectionString;
        }

        public List<Manager> LoadAll()
        {
            List<Manager> managers = new List<Manager>();
            using (SqlConnection cn = new SqlConnection(_cnStr))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Managers";
                cmd.Connection = cn;

                cn.Open();
                using (SqlDataReader dr  = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Manager newManager = new Manager();
                        newManager.ManagerId = (int)dr["ManagerId"];
                        newManager.FirstName = (string)dr["FirstName"];
                        newManager.LastName = dr["LastName"].ToString();
                        managers.Add(newManager);
                    }
                }
            }
            return managers;
        }

        public Manager Load(int managerId)
        {
            Manager manager = new Manager();

            using (var cn = new SqlConnection(Settings.ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "SELECT m.ManagerId, m.FirstName, m.LastName " +
                                  "FROM Managers m WHERE m.ManagerId = @ManagerId";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@ManagerId", managerId);

                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        manager = new Manager();
                        manager.ManagerId = (int) dr["ManagerId"];
                        manager.FirstName = dr["FirstName"].ToString();
                        manager.LastName = dr["LastName"].ToString();
                    }
                }
            }
            return manager;
        }

        public Manager Add(Manager managerToAdd)
        {
            //new Manager();

            using (var cn = new SqlConnection(Settings.ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "INSERT INTO Managers (FirstName, LastName)" +
                                  "VALUES (@Firstname, @LastName)";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@FirstName", managerToAdd.FirstName);
                cmd.Parameters.AddWithValue("@LastName", managerToAdd.LastName);

                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var manager = new Manager();
                        manager.FirstName = (string)dr["FirstName"];
                        manager.LastName = dr["LastName"].ToString();
                        managerToAdd = manager;
                    }
                }
            }
            return managerToAdd;
        }

        public Manager Edit(int managerId, Manager managerToEdit)
        {
            //new Manager();

            using (var cn = new SqlConnection(Settings.ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "UPDATE Managers SET FirstName = @FirstName, LastName = @LastName " +
                                  "WHERE ManagerId = @ManagerId";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@ManagerId", managerToEdit.ManagerId);
                cmd.Parameters.AddWithValue("@FirstName", managerToEdit.FirstName);
                cmd.Parameters.AddWithValue("@Lastname", managerToEdit.LastName);

                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var manager = new Manager();
                        manager.ManagerId = (int) dr["ManagerId"];
                        manager.FirstName = (string) dr["FirstName"];
                        manager.LastName = (string) dr["Lastname"];
                        managerToEdit = manager;
                    }
                }
            }
            return managerToEdit;
        }

        public Manager Remove(int managerId)
        {
            Manager manager = new Manager();
            using (var cn = new SqlConnection(Settings.ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "DELETE Managers WHERE ManagerId = @ManagerId";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@ManagerId", managerId);

                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Manager mgr = new Manager();
                        mgr.ManagerId = (int)dr["ManagerId"];
                        mgr.FirstName = (string)dr["FirstName"];
                        mgr.LastName = (string)dr["Lastname"];
                        manager = mgr;
                    }
                }
            }
            return manager;
        }
    }
}