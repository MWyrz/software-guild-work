﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SGCorp.Models;
using System.Web;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Linq;
using SGCorp.Models.Models;

namespace SGCorp.Data.Repositories
{
    public class ApplicantsRepo
    {
        private string _fileName = HostingEnvironment.MapPath("/App_Data/Applicants.xml");


        public List<Applicant> GetAll()
        {
            var xElement = XDocument.Load(_fileName).Root;
            if (xElement != null)
            {
                var applicantList = (from e in xElement.Elements("Applicant")
                    select new Applicant()
                    {
                        IdNumber = (int) e.Element("IdNumber"),
                        Name = (string) e.Element("Name"),
                        PhoneNumber = (string) e.Element("PhoneNumber"),
                        EmailAddress = (string) e.Element("EmailAddress"),
                        WorkExperience = (string) e.Element("WorkExperience"),
                        Education = (string) e.Element("Education"),
                        References = (string) e.Element("References"),
                        AdditionalInfo = (string) e.Element("AdditionalInfo")
                    }).ToList();
             

                return applicantList;
            }
            return null;
        }

        public Applicant GetById(int id)
        {
            var applicantList = GetAll();

            var applicant = applicantList.FirstOrDefault(a => a.IdNumber == id);

            return applicant;
        }

        public void Add(Applicant applicant)
        {
            var applicantList = GetAll();

            var maxId = applicantList.Max(m => m.IdNumber);
            applicant.IdNumber = maxId + 1;
            
            applicantList.Add(applicant);

            OverWrite(applicantList);
        }

        public void Delete(int id)
        {
            var applicantList = GetAll();

            applicantList.Remove(applicantList.FirstOrDefault(a => a.IdNumber == id));

            OverWrite(applicantList);
        }

        //public void Update(Applicant applicant, int id)
        //{
            
        //}

        private void OverWrite(List<Applicant> applicants)
        {
            if (File.Exists(_fileName))
            {
                File.Delete(_fileName);
            }
           

            using (var writer = XmlWriter.Create(_fileName))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Applicants");


                foreach (var applicant in applicants)
                {
                    writer.WriteStartElement("Applicant");
                    writer.WriteElementString("IdNumber",applicant.IdNumber.ToString());
                    writer.WriteElementString("Name", applicant.Name);
                    writer.WriteElementString("PhoneNumber", applicant.PhoneNumber);
                    writer.WriteElementString("EmailAddress", applicant.EmailAddress);
                    writer.WriteElementString("WorkExperience", applicant.WorkExperience);
                    writer.WriteElementString("Education", applicant.Education);
                    writer.WriteElementString("References", applicant.References);
                    writer.WriteElementString("AdditionalInfo", applicant.AdditionalInfo);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
    }
}
