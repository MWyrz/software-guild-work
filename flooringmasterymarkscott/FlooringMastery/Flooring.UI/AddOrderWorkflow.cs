﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL;
using Flooring.Models;

namespace Flooring.UI
{
    public class AddOrderWorkflow
    {
        public void Execute()
        {
            string config = ConfigurationSettings.AppSettings.Get("production");
            Order order = new Order();
            OrderManager manager = new OrderManager(config);
            ProductManager prodMan = new ProductManager(config);
            StateTaxManager stateMan = new StateTaxManager(config);
            Response<Order> response = new Response<Order>();
            string date = UserPrompts.GetDateFromUser();
            order.CustomerName = UserPrompts.GetCustomerNameFromUser();
            order.State = UserPrompts.GetStateFromUser();
            if (!stateMan.CheckStateTax(order.State))
            {
                manager.AddError(DateTime.Now + ": User entered an invalid state(" + order.State + ").");
                Console.WriteLine("We cannot accept orders from " + order.State + ".");
                Console.WriteLine("Data discarded.");
            }
            else
            {
                order.ProductType = UserPrompts.GetProductTypeFromUser();
                if (!prodMan.CheckProduct(order.ProductType))
                {
                    manager.AddError(DateTime.Now + ": User entered an invalid product type(" + order.ProductType + ").");
                    Console.WriteLine("That product type is not avaliable.");
                    Console.WriteLine("Data discarded.");
                }
                else
                {

                    order.Area = UserPrompts.GetAreaFromUser();

                    response = manager.GenerateOrder(date, order);
                    manager.DisplayOrder(response.Data);
                    if (UserPrompts.GetUserConfirmation())
                    {
                        manager.AddOrder(response.Data, date);
                    }
                    else
                    {
                        Console.WriteLine("Data discarded.");
                    }
                }
            }
            UserPrompts.PressAnyKeyToContinue();
        }
    }
}
