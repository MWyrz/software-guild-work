﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.Data.Repositories;
using SGCorp.Models.Models;

namespace SGCorp.BLL
{
    public class PolicyManager
    {
        public List<Category> GetAllCategories()
        {
            PolicyRepo repo = new PolicyRepo();
            return repo.GetAll();
        } 
        //public List<string> GetFileNameByCategory(string directory)
        //{
        //    PolicyRepo repo = new PolicyRepo();
        //    return repo.GetFileList(directory);
        //}

        public Category GetByID(int id)
        {
            PolicyRepo repo = new PolicyRepo();
            return repo.GetById(id);
        }

        public void DeletePolicy(Policy policy)
        {
            PolicyRepo repo = new PolicyRepo();
            repo.DeletePolicy(policy);
        }

        public void DeleteCategory(int id)
        {
            PolicyRepo repo = new PolicyRepo();
            repo.Delete(id);
        }

        public void EditPolicy(Policy policy)
        {
            PolicyRepo repo = new PolicyRepo();
            repo.Update(policy);
        }

        public void AddPolicy(Policy policy)
        {
            PolicyRepo repo = new PolicyRepo();
            repo.AddPolicy(policy);
        }

        //    public void AddPolicy(string folder, string fileName, List<string> content)
        //    {
        //        PolicyRepo repo = new PolicyRepo();
        //        repo.AddPolicy(folder, fileName, content);
        //    }
    }
}
