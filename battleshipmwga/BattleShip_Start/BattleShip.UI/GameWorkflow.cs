﻿using System;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;

namespace BattleShip.UI
{
    public class GameWorkflow
    {
        Board board1 = new Board();
        Board board2 = new Board();
        XCoordinateTranslator translator = new XCoordinateTranslator();

        public void SetUp(Player player)
        {
            DrawBoard(board1);
            PlaceShipRequest request = new PlaceShipRequest();
            ShipPlacement check;
            for (int i = 0; i < 5; i++)
            {
                while (true)
                {
                    request.Coordinate = GetShipCoords(i, player);
                    request.Direction = GetShipDirection();
                    request.ShipType = GetShipType(i);
                    if (player.PlayerNumber == "1")
                    {
                        check = board1.PlaceShip(request);
                        if (check == ShipPlacement.NotEnoughSpace)
                        {
                            Console.WriteLine("The world is flat... (there is not enough space to place the ship there).");
                        }
                        if (check == ShipPlacement.Overlap)
                        {
                            Console.WriteLine("That would overlap with an already placed ship.");
                        }
                        if (check == ShipPlacement.Ok)
                        {
                            break;
                        }
                    }
                    else
                    {
                        check = board2.PlaceShip(request);
                        if (check == ShipPlacement.NotEnoughSpace)
                        {
                            Console.WriteLine("The world is flat... (there is not enough space to place the ship there).");
                        }
                        if (check == ShipPlacement.Overlap)
                        {
                            Console.WriteLine("That would overlap with an already placed ship.");
                        }
                        if (check == ShipPlacement.Ok)
                        {
                            break;
                        }
                    }
                }
            }
        }

        public bool StartTurn(int turn, string name)
        {
            if (turn % 2 == 1)
            {
                DrawBoard(board2);
                return Fire(board2, name);
            }
            else
            {
                DrawBoard(board1);
                return Fire(board1, name);
            }
        }

        private void DrawBoard(Board board)
        {
            Console.WriteLine("|   |   |   |   |   |   |   |   |   |   |   |");
            Console.WriteLine("|   | A | B | C | D | E | F | G | H | I | J |");
            Console.WriteLine("|___|___|___|___|___|___|___|___|___|___|___|");
            for (int i = 1; i < 11; i++)
            {
                Console.WriteLine("|   |   |   |   |   |   |   |   |   |   |   |");
                if (i < 10)
                {
                    Console.Write("| {0} |", i);
                }
                else
                {
                    Console.Write("|{0} |", i);
                }
                DrawRows(board,i);
                Console.WriteLine("|___|___|___|___|___|___|___|___|___|___|___|");
            }
        }

        private void DrawRows(Board board, int y)
        {
            ShotHistory square;
            for (int i = 1; i < 11; i++)
            {
                square = GetSquareStatus(board, i, y);
                if (square == ShotHistory.Hit)
                {
                    Console.Write(" ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("H");
                    Console.ResetColor();
                    Console.Write(" |");
                }
                else if (square == ShotHistory.Miss)
                {
                    Console.Write(" ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("M");
                    Console.ResetColor();
                    Console.Write(" |");
                }
                else
                {
                    Console.Write("   |");
                }
            }
            Console.WriteLine();
        }

        private ShotHistory GetSquareStatus(Board board, int x, int y)
        {
            ShotHistory result = ShotHistory.Unknown;
            Coordinate square = new Coordinate(x,y);
            if (board.ShotHistory.TryGetValue(square, out result))
            {
                return result;
            }
            return ShotHistory.Unknown;
        }

        private Coordinate GetShipCoords(int ship, Player player)
        {
            string input;
            int[] coordinates = new int[2];
            bool properInput = false;
            do
            {
                Console.Write("{0} enter starting coordinate for your {1}: ", player.PlayerName, Enum.GetName(typeof (ShipType), ship));
                input = Console.ReadLine();
                properInput = CheckCoord(input);
            } while (!properInput);
            coordinates = translator.Translate(input);
            Coordinate shipStart = new Coordinate(coordinates[0],coordinates[1]);
            return shipStart;
        }

        private bool CheckCoord(string input)
        {
            bool properInput = translator.TranslateCheck(input);
            if (!properInput)
            {
                Console.WriteLine("You entered an incorrect coordinate.");
            }
            return properInput;
        }

        private ShipDirection GetShipDirection()
        {
            string input = "";
            bool properDirection = false;
            do
            {
                Console.Write("Enter a direction to place your ship: ");
                input = Console.ReadLine().ToUpper();
                switch (input)
                {
                    case "LEFT":
                        return ShipDirection.Left;
                    case "RIGHT":
                        return ShipDirection.Right;
                    case "DOWN":
                        return ShipDirection.Down;
                    case "UP":
                        return ShipDirection.Up;
                }
                Console.WriteLine("Input a proper direction(Up, Down, Left, Right).");
            } while (!properDirection);
            return ShipDirection.Down;
        }

        private ShipType GetShipType(int i)
        {
            switch (i)
            {
                case 0:
                    return ShipType.Destroyer;
                case 1:
                    return ShipType.Submarine;
                case 2:
                    return ShipType.Cruiser;
                case 3:
                    return ShipType.Battleship;
                case 4:
                    return ShipType.Carrier;
            }
            return ShipType.Destroyer;
        }

        private bool Fire(Board board, string name)
        {
            string playerInput;
            FireShotResponse response = new FireShotResponse();
            while (true)
            {
                int[] coords = new int[2];
                bool properInput = false;
                do {
                    Console.Write("{0} enter a coordinate to fire upon: ",name);
                    playerInput = Console.ReadLine();
                    properInput = CheckCoord(playerInput);
                } while (!properInput) ;
                coords = translator.Translate(playerInput);
                response = board.FireShot(new Coordinate(coords[0],coords[1]));
                if (response.ShotStatus == ShotStatus.Duplicate)
                {
                    Console.WriteLine("You have already fired on this location!");
                }
                if (response.ShotStatus == ShotStatus.Miss)
                {
                    Console.Clear();
                    DrawBoard(board);
                    Console.WriteLine("You hit ocean(Miss!)");
                    break;
                }
                if (response.ShotStatus == ShotStatus.Hit)
                {
                    Console.Clear();
                    DrawBoard(board);
                    Console.WriteLine("Your shot connects with one of your opponent's ships!");
                    break;
                }
                if (response.ShotStatus == ShotStatus.HitAndSunk)
                {
                    Console.Clear();
                    DrawBoard(board);
                    Console.WriteLine("Your shot connects with and sinks your opponent's {0}!", response.ShipImpacted);
                    break;
                }
                if (response.ShotStatus == ShotStatus.Victory)
                {
                    Console.Clear();
                    DrawBoard(board);
                    Console.WriteLine("Your shot connects with and sinks your opponent's final ship!");
                    return true;
                }
                if (response.ShotStatus == ShotStatus.Invalid)
                {
                    Console.WriteLine("You have entered an invalid coordinate.");
                }
            }
            return false;
        }
    }
}