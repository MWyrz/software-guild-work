USE [TestDvdLibrary]
GO
/****** Object:  StoredProcedure [dbo].[RemoveDvd]    Script Date: 2/26/2016 11:12:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[RemoveDvd]
(
   @DvdId int
) AS

DELETE FROM DvdActors WHERE DvdId = @DvdId
DELETE FROM DvdDirectors WHERE DvdId = @DvdId
DELETE FROM BorrowerReview WHERE DvdId = @DvdId
DELETE FROM Dvd WHERE DvdId = @DvdId