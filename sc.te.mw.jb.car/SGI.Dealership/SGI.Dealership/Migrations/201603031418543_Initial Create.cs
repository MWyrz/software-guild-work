namespace SGI.Dealership.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InformationRequests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequestStatusId = c.Int(nullable: false),
                        VehicleId = c.Int(nullable: false),
                        DateSubmitted = c.DateTime(nullable: false),
                        LastContactDate = c.DateTime(nullable: false),
                        CustomerName = c.String(),
                        CustomerPhone = c.String(),
                        CustomerBestTimeToContact = c.String(),
                        CustomerPreferredContactMethod = c.String(),
                        CustomerTimeFrameToBuy = c.String(),
                        CustomerAdditionalInformation = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RequestStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Vehicles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Make = c.String(),
                        Model = c.String(),
                        Year = c.Int(nullable: false),
                        Condition = c.String(),
                        Mileage = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AdTitle = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Vehicles");
            DropTable("dbo.RequestStatus");
            DropTable("dbo.InformationRequests");
        }
    }
}
