﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.Models
{
    public interface IDataSelector
    {
        List<string> GetData();
        void AddData(List<Order> orders);
        void WriteErrors(string newError);
    }
}
