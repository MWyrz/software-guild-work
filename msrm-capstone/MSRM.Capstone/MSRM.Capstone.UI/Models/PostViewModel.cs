﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.UI.Models
{
    public class PostViewModel
    {
        public Post Post { get; set; }
        public string CategoryString { get; set; }
        public string TagString { get; set; }
    }
}