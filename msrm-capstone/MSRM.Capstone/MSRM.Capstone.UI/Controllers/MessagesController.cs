﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BotDetect.Web.Mvc;
using MSRM.Capstone.BLL;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.UI.Controllers
{
    public class MessagesController : ApiController
    {
        private MessageManager _manager { get; set; }

        public MessagesController()
        {
            _manager = new MessageManager();
        }

        // GET: api/Messages
        public IEnumerable<Message> Get()
        {
            return _manager.GetAllMessages().Data;
        }

        // GET: api/Messages/5
        public Message GetMsg(int id)
        {
            var msg = _manager.GetMessageFromId(id).Data;

            return msg;
        }

        // POST: api/Messages
        [AllowAnonymous]
        [CaptchaValidation("CaptchaCode", "ExampleCaptcha", "Incorrect CAPTCHA Code!")]
        public HttpResponseMessage Post([FromBody]Message newMsg)
        {
            if (ModelState.IsValid)
            {
                var response = _manager.CreateMessage(newMsg);
                if (response.Success)
                {
                    var resp = Request.CreateResponse(HttpStatusCode.Created, response.Data);
                    string uri = Url.Link("DefaultApi", new {id = newMsg.MessageId});
                    resp.Headers.Location = new Uri(uri);

                    return resp;
                }
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, response.Message);
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid Request.");
        }

        // PUT: api/Messages/5
        public HttpResponseMessage Put(int id, Message updateMsg)
        {
                var response = _manager.UpdateMessage(updateMsg);
                if (response.Success)
                {
                    var resp = Request.CreateResponse(HttpStatusCode.Created, response.Data);
                    string uri = Url.Link("DefaultApi", new { id = updateMsg.MessageId });
                    resp.Headers.Location = new Uri(uri);

                    return resp;
                }
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, response.Message);
}
        // DELETE: api/Messages/5
        public HttpResponseMessage Delete(int id)
        {
            if (_manager.GetMessageFromId(id).Data != null)
            {
                _manager.DeleteMessage(id);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }
    }
}
