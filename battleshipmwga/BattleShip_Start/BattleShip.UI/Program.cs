﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            bool playAgain;
            Console.SetWindowSize(100,50);
            do
            {
                Program p = new Program();
                playAgain = p.Play();
            } while (playAgain);
        }

        public bool Play()
        {
            Player player1 = new Player();
            Player player2 = new Player();
            player1.PlayerNumber = "1";
            player2.PlayerNumber = "2";
            GameWorkflow work = new GameWorkflow();
            bool gameOver = false;
            int turn = 1;
            Console.WriteLine("Welcome to Battleship!!!!");
            Console.Write("Player 1, to commandeer your crew (ie get started!), enter name:");
            player1.PlayerName = Console.ReadLine();
            Console.Write("Player 2, you land-lubber!!! Tell us your name and chance the high seas!! Enter name:");
            player2.PlayerName = Console.ReadLine();
            Console.Clear();
            work.SetUp(player1);
            Console.Clear();
            work.SetUp(player2);
            Console.Clear();
            while (!gameOver)
            {
                if (turn % 2 == 1)
                {
                    gameOver = work.StartTurn(turn, player1.PlayerName);
                }
                else
                {
                    gameOver = work.StartTurn(turn, player2.PlayerName);
                }
                Console.ReadLine();
                Console.Clear();
                turn++;
            }
            if (turn % 2 == 0)
            {
                Console.WriteLine("Congratulations {0} you have won!", player1.PlayerName);
            }
            else
            {
                Console.WriteLine("Congratulations {0} you have won!", player2.PlayerName);
            }
            Console.WriteLine("\nWould you like to play again?(y/n)");
            string again = Console.ReadLine();
            if (again.ToUpper() == "Y")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
