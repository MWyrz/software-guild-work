USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[CreateInquiry]    Script Date: 3/21/2016 12:00:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[CreateInquiry]
(
	@InquiryId int output,
	@Status bit,
	@FirstName nvarchar(25),
	@LastName nvarchar(25),
	@Date date,
	@Message nvarchar(max),
	@EmailAddress nvarchar(50)
)
AS

Insert Into Inquiries (MessageStatus, FirstName, LastName, InquiryDate, Inquiry, EmailAddress)
Values (@Status, @FirstName, @LastName, @Date, @Message, @EmailAddress)
SET @InquiryId = SCOPE_IDENTITY();


GO

