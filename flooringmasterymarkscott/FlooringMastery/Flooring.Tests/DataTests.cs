﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL;
using Flooring.Data;
using Flooring.Models;
using NUnit.Framework;

namespace Flooring.Tests
{
    [TestFixture]

    public class DataTests
    {
        [TestCase("01/01/2015", "orders", 6)]
        [TestCase("02/05/2017", "orders", 0)]
        public void GetDataTest(string file, string toRead, int expected)
        {
            MockDB mockDb = new MockDB(file, toRead);

            List<string> result = mockDb.GetData();

            Assert.AreEqual(expected,result.Count);
        }

        [Test]
        public void AddDataTest()
        {
            MockDB mockDb = new MockDB("02/05/2016", "orders");
            OrderManager manager = new OrderManager("false");
            List<Order> orders = manager.GetOrders(mockDb.GetData());
            Order toAdd = new Order();
            toAdd.CustomerName = "Test";
            toAdd.State = "OH";
            toAdd.ProductType = "Tile";
            toAdd.Area = 100;
            manager.GenerateOrder("02/05/2016", toAdd);

            orders.Add(toAdd);
            mockDb.AddData(orders);

            Assert.AreEqual(orders.Count,mockDb.GetData().Count);
        }

    }
}
