﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Flooring.UI
{
    public static class UserPrompts
    {
        public static string GetDateFromUser()
        {
            string input = "";
            DateTime result;
            do
            {
                Console.Write("Enter Date(MM/DD/YYYY): ");
                input = Console.ReadLine();
                if (input == "")
                {
                    continue;
                }
                if (DateTime.TryParse(input, out result))
                {
                    
                    break;
                }
                else
                {
                    Console.WriteLine("\nThat was not a proper date.");
                }
            } while (true);
            return input;
        }

        public static void PressAnyKeyToContinue()
        {
            Console.Write("Press any key to continue.");
            Console.ReadKey();
            Console.Clear();
        }

        public static int GetOrderNumberFromUser()
        {
            int result;
            do
            {
                Console.Write("Enter Order Number: ");
                string input = Console.ReadLine();

                if (input == "")
                {
                    continue;
                }

                if (int.TryParse(input, out result))
                {
                    return result;
                }
                else
                {
                    Console.WriteLine("\nThat was not a proper order number.");
                    PressAnyKeyToContinue();
                }
            } while (true);
        }

        public static string GetCustomerNameFromUser()
        {
            while (true)
            {
                Console.Write("Enter Customer Name: ");
                string input = Console.ReadLine();

                if (input != "")
                {
                    return input;
                }
            }
        }

        public static decimal GetAreaFromUser()
        {
            decimal result;
            do
            {
                Console.Write("Enter Area: ");
                string input = Console.ReadLine();
                if (input == "")
                {
                    continue;
                }
                if (decimal.TryParse(input, out result))
                {
                    if (result > 0)
                    {
                        return result;
                    }
                    else
                    {
                        Console.WriteLine("\nArea must be greater than zero (0).");
                    }
                    
                }
                else
                {
                    Console.WriteLine("\nThat was not a proper value for area.");
                }
            } while (true);
        }

        public static string GetStateFromUser()
        {
            while (true)
            {
                Console.Write("Enter State: ");
                string input = Console.ReadLine();

                if (input != "")
                {
                    if (input.Length > 2)
                    {
                        return input.Substring(0, 1).ToUpper() + input.Substring(1);
                    }
                    else
                    {
                        return input.ToUpper();
                    }
                    
                }
            }
        }

        public static string GetProductTypeFromUser()
        {
            while (true)
            {
                Console.Write("Enter Product Type: ");
                string input = Console.ReadLine();

                if (input != "")
                {
                    return input.Substring(0, 1).ToUpper() + input.Substring(1);
                }
            }
        }

        public static bool GetUserConfirmation()
        {
            while (true)
            {
                Console.Write("Is this correct(y/n)? ");
                string input = Console.ReadLine();
                if (input.ToUpper() == "Y")
                {
                    return true;
                }
                if (input.ToUpper() == "N")
                {
                    return false;
                }
                Console.WriteLine("\nPlease enter y or n.");
            }
        }
    }
}
