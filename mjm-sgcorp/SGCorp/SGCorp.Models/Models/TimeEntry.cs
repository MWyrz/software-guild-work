﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCorp.Models.Models
{
    public class TimeEntry
    {
        public int EntryId { get; set; }
        [Required(ErrorMessage = "Enter hours worked")]
        public int HourWorked { get; set; }
        [Required(ErrorMessage = "Date is required")]
        [DataType(DataType.Date)]
        public DateTime TodaysDate { get; set; }    
        public int EmpId { get; set; }
    }
}
