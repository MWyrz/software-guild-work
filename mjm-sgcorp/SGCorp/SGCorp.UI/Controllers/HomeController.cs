﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGCorp.BLL;
using SGCorp.Models;
using SGCorp.Models.Models;

namespace SGCorp.UI.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddApplication()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddApplication(Applicant applicant)
        {
            if (ModelState.IsValid)
            {
                var manager = new ApplicantManager();

                manager.AddApplicant(applicant);
                return View("AddApplicationConfirm");
            }
            return View(applicant);

        }

        public ActionResult AddApplicationConfirm()
        {
            return View();
        }

        public ActionResult ViewApplicants()
        {
            var manager = new ApplicantManager();
            var list = manager.GetAllApplicants();
            
            return View(list.Data);
        }

        public ActionResult ViewSingleApplicant(int id)
        {
            var manager = new ApplicantManager();
            var applicant = manager.GetSingleApplicant(id);

            return View(applicant.Data);

        }
    }
}