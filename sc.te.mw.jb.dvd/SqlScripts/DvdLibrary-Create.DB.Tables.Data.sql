USE [master]
GO
/****** Object:  Database [DvdLibrary]    Script Date: 2/22/2016 4:16:04 PM ******/
CREATE DATABASE [DvdLibrary]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DvdLibrary', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\DvdLibrary.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DvdLibrary_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\DvdLibrary_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DvdLibrary] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DvdLibrary].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DvdLibrary] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DvdLibrary] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DvdLibrary] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DvdLibrary] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DvdLibrary] SET ARITHABORT OFF 
GO
ALTER DATABASE [DvdLibrary] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DvdLibrary] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DvdLibrary] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DvdLibrary] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DvdLibrary] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DvdLibrary] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DvdLibrary] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DvdLibrary] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DvdLibrary] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DvdLibrary] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DvdLibrary] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DvdLibrary] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DvdLibrary] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DvdLibrary] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DvdLibrary] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DvdLibrary] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DvdLibrary] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DvdLibrary] SET RECOVERY FULL 
GO
ALTER DATABASE [DvdLibrary] SET  MULTI_USER 
GO
ALTER DATABASE [DvdLibrary] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DvdLibrary] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DvdLibrary] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DvdLibrary] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DvdLibrary] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'DvdLibrary', N'ON'
GO
USE [DvdLibrary]
GO
/****** Object:  Table [dbo].[Actors]    Script Date: 2/22/2016 4:16:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actors](
	[ActorId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Actors] PRIMARY KEY CLUSTERED 
(
	[ActorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BorrowerReview]    Script Date: 2/22/2016 4:16:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BorrowerReview](
	[BorReviewId] [int] IDENTITY(1,1) NOT NULL,
	[DvdId] [int] NOT NULL,
	[Notes] [nvarchar](50) NULL,
	[Rating] [nvarchar](50) NULL,
 CONSTRAINT [PK_BorrowerReview] PRIMARY KEY CLUSTERED 
(
	[BorReviewId] ASC,
	[DvdId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Borrowers]    Script Date: 2/22/2016 4:16:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Borrowers](
	[BorrowerId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Borrowers] PRIMARY KEY CLUSTERED 
(
	[BorrowerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Directors]    Script Date: 2/22/2016 4:16:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Directors](
	[DirectorId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Directors] PRIMARY KEY CLUSTERED 
(
	[DirectorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Dvd]    Script Date: 2/22/2016 4:16:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dvd](
	[DvdId] [int] IDENTITY(1,1) NOT NULL,
	[StudioId] [int] NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[ReleaseDate] [date] NOT NULL,
	[MPAARating] [nvarchar](50) NULL,
 CONSTRAINT [PK_Dvd] PRIMARY KEY CLUSTERED 
(
	[DvdId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DvdActors]    Script Date: 2/22/2016 4:16:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DvdActors](
	[DvdId] [int] NOT NULL,
	[ActorId] [int] NOT NULL,
 CONSTRAINT [PK_DvdActors] PRIMARY KEY CLUSTERED 
(
	[DvdId] ASC,
	[ActorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DvdBorrowersInfo]    Script Date: 2/22/2016 4:16:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DvdBorrowersInfo](
	[BorrowerId] [int] NOT NULL,
	[DvdId] [int] NOT NULL,
	[DateBorrowed] [date] NOT NULL,
	[DateReturned] [date] NULL,
 CONSTRAINT [PK_DvdBorrowersInfo] PRIMARY KEY CLUSTERED 
(
	[BorrowerId] ASC,
	[DvdId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DvdDirectors]    Script Date: 2/22/2016 4:16:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DvdDirectors](
	[DvdId] [int] NOT NULL,
	[DirectorId] [int] NOT NULL,
 CONSTRAINT [PK_DvdDirectors] PRIMARY KEY CLUSTERED 
(
	[DvdId] ASC,
	[DirectorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Studio]    Script Date: 2/22/2016 4:16:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Studio](
	[StudioId] [int] IDENTITY(1,1) NOT NULL,
	[StudioName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Studio] PRIMARY KEY CLUSTERED 
(
	[StudioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Actors] ON 

INSERT [dbo].[Actors] ([ActorId], [FirstName], [LastName]) VALUES (1, N'Robert', N'Downey Jr.')
INSERT [dbo].[Actors] ([ActorId], [FirstName], [LastName]) VALUES (2, N'Arnold', N'Schwarzenegger')
INSERT [dbo].[Actors] ([ActorId], [FirstName], [LastName]) VALUES (3, N'Mike', N'Myers')
INSERT [dbo].[Actors] ([ActorId], [FirstName], [LastName]) VALUES (4, N'Pierce', N'Brosnan')
INSERT [dbo].[Actors] ([ActorId], [FirstName], [LastName]) VALUES (5, N'Sheri Moon', N'Zombie')
SET IDENTITY_INSERT [dbo].[Actors] OFF
SET IDENTITY_INSERT [dbo].[BorrowerReview] ON 

INSERT [dbo].[BorrowerReview] ([BorReviewId], [DvdId], [Notes], [Rating]) VALUES (1, 1, N'Good Movie. Go Iron Man', N'9/10')
INSERT [dbo].[BorrowerReview] ([BorReviewId], [DvdId], [Notes], [Rating]) VALUES (2, 10, N'Scary', N'7/10')
INSERT [dbo].[BorrowerReview] ([BorReviewId], [DvdId], [Notes], [Rating]) VALUES (3, 7, N'It was Good, Very Good', N'8/10')
INSERT [dbo].[BorrowerReview] ([BorReviewId], [DvdId], [Notes], [Rating]) VALUES (4, 3, N'Go Arnold', N'7/10')
SET IDENTITY_INSERT [dbo].[BorrowerReview] OFF
SET IDENTITY_INSERT [dbo].[Borrowers] ON 

INSERT [dbo].[Borrowers] ([BorrowerId], [FirstName], [LastName]) VALUES (1, N'Jake', N'Bosak')
INSERT [dbo].[Borrowers] ([BorrowerId], [FirstName], [LastName]) VALUES (2, N'Ted', N'Ebenger')
INSERT [dbo].[Borrowers] ([BorrowerId], [FirstName], [LastName]) VALUES (3, N'Scott', N'Certain')
INSERT [dbo].[Borrowers] ([BorrowerId], [FirstName], [LastName]) VALUES (4, N'Mark', N'Wyrzykowski')
SET IDENTITY_INSERT [dbo].[Borrowers] OFF
SET IDENTITY_INSERT [dbo].[Directors] ON 

INSERT [dbo].[Directors] ([DirectorId], [FirstName], [LastName]) VALUES (1, N'Jon', N'Favreau')
INSERT [dbo].[Directors] ([DirectorId], [FirstName], [LastName]) VALUES (2, N'Joss', N'Whedon')
INSERT [dbo].[Directors] ([DirectorId], [FirstName], [LastName]) VALUES (3, N'James', N'Cameron')
INSERT [dbo].[Directors] ([DirectorId], [FirstName], [LastName]) VALUES (4, N'John', N'McTiernan')
INSERT [dbo].[Directors] ([DirectorId], [FirstName], [LastName]) VALUES (5, N'Andrew', N'Adamson')
INSERT [dbo].[Directors] ([DirectorId], [FirstName], [LastName]) VALUES (6, N'Penelope', N'Spheeris')
INSERT [dbo].[Directors] ([DirectorId], [FirstName], [LastName]) VALUES (7, N'Martin', N'Campbell')
INSERT [dbo].[Directors] ([DirectorId], [FirstName], [LastName]) VALUES (8, N'Roger', N'Spottiswoode')
INSERT [dbo].[Directors] ([DirectorId], [FirstName], [LastName]) VALUES (9, N'Rob', N'Zombie')
SET IDENTITY_INSERT [dbo].[Directors] OFF
SET IDENTITY_INSERT [dbo].[Dvd] ON 

INSERT [dbo].[Dvd] ([DvdId], [StudioId], [Title], [ReleaseDate], [MPAARating]) VALUES (1, 1, N'Iron Man', CAST(N'2008-05-02' AS Date), N'PG-13')
INSERT [dbo].[Dvd] ([DvdId], [StudioId], [Title], [ReleaseDate], [MPAARating]) VALUES (2, 1, N'Marvels Avengers', CAST(N'2012-05-04' AS Date), N'PG-13')
INSERT [dbo].[Dvd] ([DvdId], [StudioId], [Title], [ReleaseDate], [MPAARating]) VALUES (3, 2, N'Ternimator 2: Judgement Day', CAST(N'1991-07-03' AS Date), N'PG-13')
INSERT [dbo].[Dvd] ([DvdId], [StudioId], [Title], [ReleaseDate], [MPAARating]) VALUES (4, 3, N'Predator', CAST(N'1987-06-12' AS Date), N'R')
INSERT [dbo].[Dvd] ([DvdId], [StudioId], [Title], [ReleaseDate], [MPAARating]) VALUES (5, 4, N'Shrek', CAST(N'2001-04-22' AS Date), N'PG')
INSERT [dbo].[Dvd] ([DvdId], [StudioId], [Title], [ReleaseDate], [MPAARating]) VALUES (6, 5, N'Waynes World', CAST(N'1992-02-14' AS Date), N'PG')
INSERT [dbo].[Dvd] ([DvdId], [StudioId], [Title], [ReleaseDate], [MPAARating]) VALUES (7, 6, N'007: GoldenEye', CAST(N'1997-11-17' AS Date), N'PG-13')
INSERT [dbo].[Dvd] ([DvdId], [StudioId], [Title], [ReleaseDate], [MPAARating]) VALUES (8, 7, N'007: Tomorrow Never Dies', CAST(N'1997-12-09' AS Date), N'PG-13')
INSERT [dbo].[Dvd] ([DvdId], [StudioId], [Title], [ReleaseDate], [MPAARating]) VALUES (9, 8, N'The Devils Rejects', CAST(N'2005-07-22' AS Date), N'R')
INSERT [dbo].[Dvd] ([DvdId], [StudioId], [Title], [ReleaseDate], [MPAARating]) VALUES (10, 7, N'Halloween', CAST(N'2007-08-31' AS Date), N'R')
SET IDENTITY_INSERT [dbo].[Dvd] OFF
INSERT [dbo].[DvdActors] ([DvdId], [ActorId]) VALUES (1, 1)
INSERT [dbo].[DvdActors] ([DvdId], [ActorId]) VALUES (2, 1)
INSERT [dbo].[DvdActors] ([DvdId], [ActorId]) VALUES (3, 2)
INSERT [dbo].[DvdActors] ([DvdId], [ActorId]) VALUES (4, 2)
INSERT [dbo].[DvdActors] ([DvdId], [ActorId]) VALUES (5, 3)
INSERT [dbo].[DvdActors] ([DvdId], [ActorId]) VALUES (6, 3)
INSERT [dbo].[DvdActors] ([DvdId], [ActorId]) VALUES (7, 4)
INSERT [dbo].[DvdActors] ([DvdId], [ActorId]) VALUES (8, 4)
INSERT [dbo].[DvdActors] ([DvdId], [ActorId]) VALUES (9, 5)
INSERT [dbo].[DvdActors] ([DvdId], [ActorId]) VALUES (10, 5)
INSERT [dbo].[DvdBorrowersInfo] ([BorrowerId], [DvdId], [DateBorrowed], [DateReturned]) VALUES (1, 9, CAST(N'2016-02-14' AS Date), CAST(N'1900-01-01' AS Date))
INSERT [dbo].[DvdBorrowersInfo] ([BorrowerId], [DvdId], [DateBorrowed], [DateReturned]) VALUES (2, 3, CAST(N'2010-09-07' AS Date), CAST(N'2010-09-14' AS Date))
INSERT [dbo].[DvdBorrowersInfo] ([BorrowerId], [DvdId], [DateBorrowed], [DateReturned]) VALUES (3, 1, CAST(N'2008-03-20' AS Date), CAST(N'2015-03-21' AS Date))
INSERT [dbo].[DvdBorrowersInfo] ([BorrowerId], [DvdId], [DateBorrowed], [DateReturned]) VALUES (4, 4, CAST(N'2013-11-12' AS Date), CAST(N'2014-12-13' AS Date))
INSERT [dbo].[DvdDirectors] ([DvdId], [DirectorId]) VALUES (1, 1)
INSERT [dbo].[DvdDirectors] ([DvdId], [DirectorId]) VALUES (2, 2)
INSERT [dbo].[DvdDirectors] ([DvdId], [DirectorId]) VALUES (3, 3)
INSERT [dbo].[DvdDirectors] ([DvdId], [DirectorId]) VALUES (4, 4)
INSERT [dbo].[DvdDirectors] ([DvdId], [DirectorId]) VALUES (5, 5)
INSERT [dbo].[DvdDirectors] ([DvdId], [DirectorId]) VALUES (6, 6)
INSERT [dbo].[DvdDirectors] ([DvdId], [DirectorId]) VALUES (7, 7)
INSERT [dbo].[DvdDirectors] ([DvdId], [DirectorId]) VALUES (8, 8)
INSERT [dbo].[DvdDirectors] ([DvdId], [DirectorId]) VALUES (9, 9)
INSERT [dbo].[DvdDirectors] ([DvdId], [DirectorId]) VALUES (10, 9)
SET IDENTITY_INSERT [dbo].[Studio] ON 

INSERT [dbo].[Studio] ([StudioId], [StudioName]) VALUES (1, N'Marvel Studios')
INSERT [dbo].[Studio] ([StudioId], [StudioName]) VALUES (2, N'Tri Star Pictures')
INSERT [dbo].[Studio] ([StudioId], [StudioName]) VALUES (3, N'20th Centry Fox')
INSERT [dbo].[Studio] ([StudioId], [StudioName]) VALUES (4, N'Dream Works')
INSERT [dbo].[Studio] ([StudioId], [StudioName]) VALUES (5, N'Paramount Pictures')
INSERT [dbo].[Studio] ([StudioId], [StudioName]) VALUES (6, N'United Artist')
INSERT [dbo].[Studio] ([StudioId], [StudioName]) VALUES (7, N'Metro-Goldwyn-Mayer')
INSERT [dbo].[Studio] ([StudioId], [StudioName]) VALUES (8, N'LionsGate')
SET IDENTITY_INSERT [dbo].[Studio] OFF
ALTER TABLE [dbo].[BorrowerReview]  WITH CHECK ADD  CONSTRAINT [FK_BorrowerReview_Borrowers] FOREIGN KEY([BorReviewId])
REFERENCES [dbo].[Borrowers] ([BorrowerId])
GO
ALTER TABLE [dbo].[BorrowerReview] CHECK CONSTRAINT [FK_BorrowerReview_Borrowers]
GO
ALTER TABLE [dbo].[BorrowerReview]  WITH CHECK ADD  CONSTRAINT [FK_BorrowerReview_Dvd] FOREIGN KEY([DvdId])
REFERENCES [dbo].[Dvd] ([DvdId])
GO
ALTER TABLE [dbo].[BorrowerReview] CHECK CONSTRAINT [FK_BorrowerReview_Dvd]
GO
ALTER TABLE [dbo].[Dvd]  WITH CHECK ADD  CONSTRAINT [FK_Dvd_Studio] FOREIGN KEY([StudioId])
REFERENCES [dbo].[Studio] ([StudioId])
GO
ALTER TABLE [dbo].[Dvd] CHECK CONSTRAINT [FK_Dvd_Studio]
GO
ALTER TABLE [dbo].[DvdActors]  WITH CHECK ADD  CONSTRAINT [FK_DvdActors_Actors] FOREIGN KEY([ActorId])
REFERENCES [dbo].[Actors] ([ActorId])
GO
ALTER TABLE [dbo].[DvdActors] CHECK CONSTRAINT [FK_DvdActors_Actors]
GO
ALTER TABLE [dbo].[DvdActors]  WITH CHECK ADD  CONSTRAINT [FK_DvdActors_Dvd] FOREIGN KEY([DvdId])
REFERENCES [dbo].[Dvd] ([DvdId])
GO
ALTER TABLE [dbo].[DvdActors] CHECK CONSTRAINT [FK_DvdActors_Dvd]
GO
ALTER TABLE [dbo].[DvdBorrowersInfo]  WITH CHECK ADD  CONSTRAINT [FK_DvdBorrowersInfo_Borrowers] FOREIGN KEY([BorrowerId])
REFERENCES [dbo].[Borrowers] ([BorrowerId])
GO
ALTER TABLE [dbo].[DvdBorrowersInfo] CHECK CONSTRAINT [FK_DvdBorrowersInfo_Borrowers]
GO
ALTER TABLE [dbo].[DvdBorrowersInfo]  WITH CHECK ADD  CONSTRAINT [FK_DvdBorrowersInfo_Dvd] FOREIGN KEY([DvdId])
REFERENCES [dbo].[Dvd] ([DvdId])
GO
ALTER TABLE [dbo].[DvdBorrowersInfo] CHECK CONSTRAINT [FK_DvdBorrowersInfo_Dvd]
GO
ALTER TABLE [dbo].[DvdDirectors]  WITH CHECK ADD  CONSTRAINT [FK_DvdDirectors_Directors] FOREIGN KEY([DirectorId])
REFERENCES [dbo].[Directors] ([DirectorId])
GO
ALTER TABLE [dbo].[DvdDirectors] CHECK CONSTRAINT [FK_DvdDirectors_Directors]
GO
ALTER TABLE [dbo].[DvdDirectors]  WITH CHECK ADD  CONSTRAINT [FK_DvdDirectors_Dvd] FOREIGN KEY([DvdId])
REFERENCES [dbo].[Dvd] ([DvdId])
GO
ALTER TABLE [dbo].[DvdDirectors] CHECK CONSTRAINT [FK_DvdDirectors_Dvd]
GO
USE [master]
GO
ALTER DATABASE [DvdLibrary] SET  READ_WRITE 
GO
