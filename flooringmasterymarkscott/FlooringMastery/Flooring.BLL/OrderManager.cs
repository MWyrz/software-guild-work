﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data;
using Flooring.Models;

namespace Flooring.BLL
{
    public class OrderManager
    {
        private bool production;
        public OrderManager(string config)
        {
            if (config == "true")
            {
                production = true;
            }
            else
            {
                production = false;
            }
        }
        public Response<List<Order>> DisplayOrders(string date)
        {
            IDataSelector data;
            if (production)
            {
                data = new ProdDB(date, "orders");
            }
            else
            {
                data = new MockDB(date, "orders");
            }
            Response<List<Order>> response = new Response<List<Order>>();
            List<string> orderStrings = data.GetData();
            foreach (var order in orderStrings)
            {
                string splitter = "\",\"";
                if (order.Split(new string[] {splitter}, StringSplitOptions.None).Length != 12)
                {
                    response.Message = "Input file is formatted incorrectly.";
                    AddError(DateTime.Now + ": User entered a date(" + date + ") whose file is incorrectly formatted.");
                    response.Success = false;
                    return response;
                }
            }
            List<Order> orders = GetOrders(orderStrings);

            if (orders.Count != 0)
            {
                response.Success = true;
                response.Data = orders;
            }
            else
            {
                response.Message = "There are no orders on that date.";
                AddError(DateTime.Now + ": User entered a date(" + date + ") that has no orders.");
                response.Success = false;
            }
            return response;
        }

        public void DisplayOrder(Order order)
        {
            Console.WriteLine("\nOrder Number: {0}",order.OrderNumber);
            Console.WriteLine("Customer Name: {0}",order.CustomerName);
            Console.WriteLine("State: {0}", order.State);
            Console.WriteLine("Tax Rate: {0}%", order.TaxRate);
            Console.WriteLine("Product Type: {0}", order.ProductType);
            Console.WriteLine("Area: {0}", order.Area);
            Console.WriteLine("CostPerSquareFoot: {0:C}", order.CostPerSquareFoot);
            Console.WriteLine("Labor Cost Per Square Foot: {0:C}", order.LaborCostPerSquareFoot);
            Console.WriteLine("Material Cost: {0:C}", order.MaterialCost);
            Console.WriteLine("Labor Cost: {0:C}", order.LaborCost);
            Console.WriteLine("Tax: {0:C}", order.Tax);
            Console.WriteLine("Total: {0:C}\n", order.Total);
        }

        public Response<Order> GenerateOrder(string date, Order order)
        {
            StateTaxManager tax = new StateTaxManager(production);
            ProductManager product = new ProductManager(production);
            IDataSelector data;
            if (production)
            {
                data = new ProdDB(date, "orders");
            }
            else
            {
                data = new MockDB(date, "orders");
            }
            List<Order> orders = GetOrders(data.GetData());
            Response<Order> response = new Response<Order>();
            response.Data = order;
            if (orders.Count != 0)
            {
                response.Data.OrderNumber = orders.ToArray()[orders.Count - 1].OrderNumber + 1;
            }
            else
            {
                response.Data.OrderNumber = 1;
            }
            decimal[] pArray = product.GetProductPricing(response.Data.ProductType);
            if (pArray.Length != 0)
            {
                response.Data.LaborCostPerSquareFoot = pArray[1];
                response.Data.CostPerSquareFoot = pArray[0];
            }
            response.Data.TaxRate = tax.GetTaxRate(response.Data.State);

            return response;
        }

        public void AddOrder(Order order, string date)
        {
            IDataSelector data;
            if (production)
            {
                data = new ProdDB(date, "orders");
            }
            else
            {
                data = new MockDB(date, "orders");
            }
            List<Order> orders = GetOrders(data.GetData());

            orders.Add(order);
            data.AddData(orders);
        }

        public void EditOrder(string date, List<Order> editedList)
        {
            IDataSelector data;
            if (production)
            {
                data = new ProdDB(date, "orders");
            }
            else
            {
                data = new MockDB(date, "orders");
            }
            data.AddData(editedList);

        }

        public void RemoveOrder(string date, Order orderToRemove)
        {
            IDataSelector data;
            if (production)
            {
                data = new ProdDB(date, "orders");
            }
            else
            {
                data = new MockDB(date, "orders");
            }
            List<Order> orders = GetOrders(data.GetData());
            List<Order> toOutput = new List<Order>();
            Response<Order> response = new Response<Order>();

            foreach (var order in orders)
            {
                if (order.OrderNumber != orderToRemove.OrderNumber)
                {
                    toOutput.Add(order);
                }
            }
            data.AddData(toOutput);
        }

        public List<Order> GetOrders(List<string> lines)
        {
            List<Order> orders = new List<Order>();
            string[] lineArray = lines.ToArray();
            for (int i = 0; i < lineArray.Length; i++)
            {
                string splitter = "\",\"";
                string[] columns = lineArray[i].Split(new string[] { splitter }, StringSplitOptions.None);
                var order = new Order();

                order.OrderNumber = int.Parse(columns[0]);
                order.CustomerName = columns[1].Replace(",,",",");
                order.State = columns[2];
                order.TaxRate = decimal.Parse(columns[3]);
                order.ProductType = columns[4];
                order.Area = decimal.Parse(columns[5]);
                order.CostPerSquareFoot = decimal.Parse(columns[6]);
                order.LaborCostPerSquareFoot = decimal.Parse(columns[7]);

                orders.Add(order);
            }
            return orders;
        }

        public void AddError(string error)
        {
            IDataSelector data;
            if (production)
            {
                data = new ProdDB("", "error");
            }
            else
            {
                data = new MockDB("", "error");
            }
            data.WriteErrors(error);
        } 
    }
}
