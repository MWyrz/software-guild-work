OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total
1","George ofthe Jungle","IN","6.00","Wood","500","5.15","4.75","2575.00","2375.00","297.0000","5247.0000
3","Swank  Hillary","MI","5.75","Wood","10000","5.15","4.75","51500.00","47500.00","5692.500000","104692.500000
4","Ahhnold Schwarzengruuuben","PA","6.75","Tile","100000","3.50","4.15","350000.00","415000.00","51637.500000","816637.500000
5","Rowan Atkinson aka Mr Bean","Indiana","6.00","Laminate","750","1.75","2.10","1312.50","1575.00","173.2500","3060.7500
6","GE,, INC","IN","6.00","Tile","10000","3.50","4.15","35000.00","41500.00","4590.0000","81090.0000
7","General Electric,, Inc","OH","6.25","Carpet","10000","2.25","2.10","22500.00","21000.00","2718.750000","46218.750000
