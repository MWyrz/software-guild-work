﻿using System.Collections.Generic;
using BaseballLeague.MODELS;

namespace BaseballLeague.CONTRACTS.Repository
{
    public interface IManagerRepository
    {
        Manager Add(Manager managerToAdd);
        List<Manager> LoadAll();
        Manager Load(int managerId);
        Manager Edit(int managerId, Manager managerToEdit);
        Manager Remove(int managerId);
    }
}
