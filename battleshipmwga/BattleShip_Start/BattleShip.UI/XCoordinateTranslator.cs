﻿using System.Collections.Generic;
using BattleShip.BLL.Requests;

namespace BattleShip.UI
{
    public class XCoordinateTranslator
    {
        Dictionary<string, int> translate = new Dictionary<string, int>()
        {
            {"A", 1},
            {"B", 2},
            {"C", 3},
            {"D", 4},
            {"E", 5},
            {"F", 6},
            {"G", 7},
            {"H", 8},
            {"I", 9},
            {"J", 10}
        };

        public bool TranslateCheck(string input)
        {
            int output;
            if (input == "")
                return false;
            if (translate.TryGetValue(input.Substring(0, 1).ToUpper(), out output))
            {
                int.TryParse(input.Substring(1), out output);
                if (output < 11 && output > 0)
                {
                    return int.TryParse(input.Substring(1), out output);
                }
            }
            return false;
        }

        public int[] Translate(string input)
        {
            int[] result = new int[2];
            translate.TryGetValue(input.Substring(0, 1).ToUpper(), out result[0]);
            int.TryParse(input.Substring(1), out result[1]);
            return result;
        } 
    }
}