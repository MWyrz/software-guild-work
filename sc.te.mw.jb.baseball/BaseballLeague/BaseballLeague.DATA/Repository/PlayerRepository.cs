﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using BaseballLeague.CONTRACTS.Repository;
using BaseballLeague.DATA.Config;
using BaseballLeague.MODELS;

namespace BaseballLeague.DATA.Repository
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly string _cnStr;

        public PlayerRepository()
        {
            _cnStr = Settings.ConnectionString;
        }

        public List<Player> LoadAll()
        {
            var players = new List<Player>();

            using (SqlConnection cn = new SqlConnection(_cnStr))
            {
                var cmd = new SqlCommand
                {
                    CommandText = "LoadAllPlayers",
                    CommandType = CommandType.StoredProcedure,
                    Connection = cn
                };

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        players.Add(PopulateFromDataReader(dr));
                    }
                }
            }
            return players;
        }

        public Player Load(int playerId)
        {
            var player = new Player();
            
            using (SqlConnection cn = new SqlConnection(_cnStr))
            {
                var cmd = new SqlCommand
                {
                    CommandText = "LoadPlayerById",
                    CommandType = CommandType.StoredProcedure,
                    Connection = cn
                };

                cmd.Parameters.AddWithValue("@PlayerId", playerId);
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        player = PopulateFromDataReader(dr);
                    }
                }

            }
            return player;
        }

        public Player Add(Player playerToAdd)
        {
            var player = new Player();

            using (SqlConnection cn = new SqlConnection(_cnStr))
            {
                var cmd = new SqlCommand
                {
                    CommandText = "AddPlayer",
                    CommandType = CommandType.StoredProcedure,
                    Connection = cn
                };
                cmd.Parameters.AddWithValue("@TeamId", playerToAdd.TeamId);
                cmd.Parameters.AddWithValue("@FirstName", playerToAdd.FirstName);
                cmd.Parameters.AddWithValue("@LastName", playerToAdd.LastName);
                cmd.Parameters.AddWithValue("@JerseyNumber", playerToAdd.JerseyNumber);
                cmd.Parameters.AddWithValue("@RookieYear", playerToAdd.RookieYear);
                if (playerToAdd.LastSeasonBatAvg == null)
                    cmd.Parameters.AddWithValue("@LastSeasonBatAvg", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LastSeasonBatAvg", playerToAdd.TeamId);
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        player = PopulateFromDataReader(dr);
                    }
                }
            }
            return player;
        }

        public Player Edit(int playerId, Player playerToEdit)
        {
            throw new NotImplementedException();
        }

        public int Remove(int playerId)
        {
            throw new NotImplementedException();
        }

        private Player PopulateFromDataReader(SqlDataReader dr)
        {
            var player = new Player()
            {
                PlayerId = (int)dr["PlayerId"],
                TeamId = (int)dr["TeamId"],
                FirstName = dr["FirstName"].ToString(),
                LastName = dr["LastName"].ToString(),
                JerseyNumber = (int)dr["JerseyNumber"],
                RookieYear = (int)dr["RookieYear"]
            };
            if (dr["LastSeasonBatAvg"] == DBNull.Value) return player;

            player.LastSeasonBatAvg = (decimal) dr["LastSeasonBatAvg"];

            return player;
        }
    }
}