﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using DvdLibrary.DATA.Repository;
using DvdLibrary.MODELS;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace DvdLibrary.TESTS
{
    [TestFixture]
    class DvdLibraryTest
    {
        private DvdRepository _dvdRepo;
        private Dvd _testDvd;

        [SetUp]
        public void TestSetUp()
        {
            _dvdRepo = new DvdRepository();
            _testDvd = new Dvd();
        }

        [TearDown]
        public void TearDown()
        {
            Utilities.RebuildTestDVd();
        }


        [Test]
        public void GetAllDvds()
        {
            var repo = new DvdRepository();
            var dvdList = repo.GetAllDvds();

            Assert.AreEqual(10,dvdList.Count);
        }

        [Test]
        public void AddDvd()
        {
            _testDvd.Studio = new Studio();
            _testDvd.Actor = new Actor();
            _testDvd.Director = new Director();
            _testDvd.Studio.StudioId = 1;
            _testDvd.Title = "Iron Man 2";
            _testDvd.ReleaseDate = DateTime.Now;
            _testDvd.MPAARating = "PG-13";
            _testDvd.Actor.ActorId = 1;
            _testDvd.Director.DirectorId = 1;

            var actual = _dvdRepo.AddDvd(_testDvd);

            Assert.AreEqual(11, _dvdRepo.GetAllDvds().Count);
            Assert.AreEqual(11, actual.DvdId);
        }

        [Test]
        public void GetDvdInfo()
        {
            var actual = _dvdRepo.GetDvdInfo(1);

            Assert.AreNotEqual(0, actual.Reviews.Count);
            Assert.AreEqual(1, actual.DvdId);
            Assert.AreEqual("Iron Man", actual.Title);
        }

        [Test]
        public void GetBorrowerInfo()
        {
            var actual = _dvdRepo.GetBorrowerInfo(1);

            Assert.AreEqual(1, actual.Count);
        }

        [Test]
        public void AddActor()
        {
            Actor actor = new Actor() {FirstName = "Mark", LastName = "Wyrzykowksi"};
            _dvdRepo.AddActor(actor);

            var actual = _dvdRepo.GetAllActors();
            Assert.AreEqual(6,actual.Count);
        }

        [Test]
        public void AddDirector()
        {
            Director director = new Director() { FirstName = "Mark", LastName = "Wyrzykowksi" };
            _dvdRepo.AddDirector(director);

            var actual = _dvdRepo.GetAllDirectors();
            Assert.AreEqual(10, actual.Count);
        }

        [Test]
        public void AddStudio()
        {
            Studio studio = new Studio() { StudioName = "DC Studios"};
            _dvdRepo.AddStudio(studio);

            var actual = _dvdRepo.GetAllStudios();
            Assert.AreEqual(9, actual.Count);
        }

        [Test]
        public void RemoveDvd()
        {
            _dvdRepo.RemoveDvd(2);
            var actual = _dvdRepo.GetAllDvds();

            Assert.AreEqual(9, actual.Count);
        }
    }
}
