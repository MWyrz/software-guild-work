﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Linq;
using SGCorp.Models.Models;

namespace SGCorp.Data.Repositories
{
    public class PolicyRepo 
    {
        private string _fileName = HostingEnvironment.MapPath("/App_Data/Policies.xml");

        public List<Category> GetAll()
        {
            var xElement = XDocument.Load(_fileName).Root;
            if (xElement != null)
            {
                var categoryList = (from e in xElement.Elements("Category")
                    select new Category()
                    {
                        CategoryId = (int) e.Element("CategoryId"),
                        CategoryName = (string) e.Element("CategoryName"),
                        Policies = (from f in e.Elements("Policy")
                            select new Policy()
                            {
                                PolicyId = (int) f.Element("PolicyId"),
                                PolicyName = (string) f.Element("PolicyName"),
                                Description = (string) f.Element("Description"),
                                CategoryName = (string) f.Element("CategoryName")
                            }).ToList()

                    }).ToList();

                return categoryList;
            }
            return null;
        }

        public Category GetById(int id)
        {
            var categoryList = GetAll();

            var category = categoryList.FirstOrDefault(a => a.CategoryId == id);

            return category;
        }

        //public void Add(Category category)
        //{
        //    var categoryList = GetAll();

        //    var maxId = categoryList.Max(m => m.CategoryId);
        //    category.CategoryId = maxId + 1;

        //    categoryList.Add(category);

        //    OverWrite(categoryList);
        //}

        public void AddPolicy(Policy policy)
        {
            var categories = GetAll();
            bool found = false;

            while (!found)
            {
                foreach (var cat in categories)
                {
                    if (cat.CategoryName == policy.CategoryName)
                    {
                        if (cat.Policies.Count != 0)
                        {
                            policy.PolicyId = cat.Policies.Max(c => c.PolicyId) + 1;
                        }
                        else
                        {
                            policy.PolicyId = 1;
                        }
                        cat.Policies.Add(policy);
                        found = true;
                    }
                }
                if (!found)
                {
                    Category category = new Category();
                    category.CategoryName = policy.CategoryName;
                    categories.Add(AddCategory(category));
                }
            }
            OverWrite(categories);
        }

        public Category AddCategory(Category category)
        {
            var categories = GetAll();
            category.Policies = new List<Policy>();
            category.CategoryId = categories.Max(c => c.CategoryId) + 1;
            return category;
        }

        public void Delete(int id)
        {
            var categoryList = GetAll();

            categoryList.Remove(categoryList.FirstOrDefault(a => a.CategoryId == id));

            OverWrite(categoryList);
        }

        public void DeletePolicy(Policy policy)
        {
            var categoryList = GetAll();
            var policyList = categoryList.FirstOrDefault(a => a.CategoryName == policy.CategoryName)?.Policies;

            policyList?.Remove(policyList.FirstOrDefault(a => a.PolicyId == policy.PolicyId));

            OverWrite(categoryList);
        }

        public void Update(Policy policy)
        {
            var categories = GetAll();
            foreach (var cat in categories)
            {
                if (cat.CategoryName == policy.CategoryName)
                {
                    foreach (var pol in cat.Policies)
                    {
                        if (pol.PolicyName == policy.PolicyName)
                        {
                            pol.Description = policy.Description;
                        }
                    }
                }
            }
            OverWrite(categories);
        }

        private void OverWrite(List<Category> categories)
        {
            if (File.Exists(_fileName))
            {
                File.Delete(_fileName);
            }

            using (var writer = XmlWriter.Create(_fileName))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Categories");


                foreach (var category in categories)
                {
                    writer.WriteStartElement("Category");
                    writer.WriteElementString("CategoryId", category.CategoryId.ToString());
                    writer.WriteElementString("CategoryName", category.CategoryName);
                   
                    foreach (var policy in category.Policies)
                    {
                        writer.WriteStartElement("Policy");
                        writer.WriteElementString("PolicyId", policy.PolicyId.ToString());
                        writer.WriteElementString("PolicyName", policy.PolicyName);
                        writer.WriteElementString("Description", policy.Description);
                        writer.WriteElementString("CategoryName", policy.CategoryName);
                        writer.WriteEndElement();
                    }
                                   
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        //public List<string> GetCategoriesList()
        //{
        //    List<string> categoryList = new List<string>();

        //    var reader = File.ReadAllLines(_fileName + "/Categories.txt");

        //    for (int i = 0; i < reader.Length; i++)
        //    {
        //        categoryList.Add(reader[i]);
        //    }

        //    return categoryList;
        //}

        //public List<string> GetFileList(string folder)
        //{
        //    List<string> fileList = new List<string>();
        //    string[] fileEntries = Directory.GetFiles(_fileName + "\\" + folder);
        //    foreach (string filename in fileEntries)
        //    {

        //        fileList.Add(filename);

        //    }
        //    return fileList;

        //}

        //public List<string> GetPolicy(string filePath)
        //{
        //    List<string> policy = new List<string>();

        //    var reader = File.ReadAllLines(filePath);

        //    for (int i = 0; i < reader.Length; i++)
        //    {
        //        policy.Add(reader[i]);
        //    }

        //    return policy;
        //}

        //public void DeletePolicy(string filePath)
        //{
        //    File.Delete(filePath);
        //}

        //public void EditPolicy(string filePath, List<string> content)
        //{
        //    if(File.Exists(filePath))
        //        File.Delete(filePath);

        //    using (var writer = File.CreateText(filePath))
        //    {
        //        foreach (var item in content)
        //        {
        //            writer.WriteLine(item);
        //        }
        //    }
        //}

        //public void AddPolicy(string folder, string fileName, List<string> content)
        //{
        //    string file = _fileName + "/" + folder + "/" + fileName;

        //    EditPolicy(file, content);
        //}


    }
}
