﻿using BattleShip.BLL.Responses;

namespace BattleShip.UI
{
    public class Player
    {
        public string PlayerName { get; set; }
        public string PlayerNumber { get; set; } 
    }
}