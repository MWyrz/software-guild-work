CREATE PROCEDURE DeletePage
(
	@StaticPageId int
) AS

DELETE FROM Pages
WHERE StaticPageID = @StaticPageId

GO

