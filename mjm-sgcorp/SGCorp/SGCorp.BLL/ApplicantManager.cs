﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.Data.Repositories;
using SGCorp.Models;
using SGCorp.Models.Models;
using SGCorp.Models.Response;

namespace SGCorp.BLL
{
    public class ApplicantManager
    {
        public Response<Applicant> GetSingleApplicant(int id)
        {
            var response = new Response<Applicant>();
            try
            {
                var repository = new ApplicantsRepo();
                var applicant = repository.GetById(id);

                if (applicant == null)
                {
                    response.Success = false;
                    response.Message = "Id could not be found";
                }
                else
                {
                    response.Success = true;
                    response.Data = applicant;
                }


            }
            catch (Exception)
            {

                response.Success = false;
                response.Message = "An error has occured, Please try again";
            }
            return response;
        }

        public Response<List<Applicant>> GetAllApplicants()
        {
            var response = new Response<List<Applicant>>();
            var repository = new ApplicantsRepo();

            try
            {
                var applicantList = repository.GetAll();
                if (applicantList == null)
                {
                    response.Success = false;
                    response.Message = "There Are No Current Applicants";
                }
                else
                {
                    response.Success = true;
                    response.Data = applicantList;
                }


            }
            catch (Exception)
            {

                response.Success = false;
                response.Message = "An error has occured, Please try again";
            }
            return response;
        }

        public void AddApplicant(Applicant applicant)
        {
            var repository = new ApplicantsRepo();
            repository.Add(applicant);
        }

    }
}
