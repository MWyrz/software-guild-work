USE [master]
GO
/****** Object:  Database [Capstone]    Script Date: 3/10/2016 9:29:26 AM ******/
CREATE DATABASE [Capstone]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Capstone', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\Capstone.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Capstone_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\Capstone_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Capstone] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Capstone].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Capstone] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Capstone] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Capstone] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Capstone] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Capstone] SET ARITHABORT OFF 
GO
ALTER DATABASE [Capstone] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Capstone] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Capstone] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Capstone] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Capstone] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Capstone] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Capstone] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Capstone] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Capstone] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Capstone] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Capstone] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Capstone] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Capstone] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Capstone] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Capstone] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Capstone] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Capstone] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Capstone] SET RECOVERY FULL 
GO
ALTER DATABASE [Capstone] SET  MULTI_USER 
GO
ALTER DATABASE [Capstone] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Capstone] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Capstone] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Capstone] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Capstone] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Capstone', N'ON'
GO
USE [Capstone]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Categories]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Inquiries]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inquiries](
	[InquiryID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](25) NOT NULL,
	[LastName] [nvarchar](25) NOT NULL,
	[InquiryDate] [date] NOT NULL,
	[Inquiry] [nvarchar](max) NOT NULL,
	[EmailAddress] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Inquiries] PRIMARY KEY CLUSTERED 
(
	[InquiryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Pages]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pages](
	[PageID] [int] IDENTITY(1,1) NOT NULL,
	[PageContent] [nvarchar](max) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Pages_1] PRIMARY KEY CLUSTERED 
(
	[PageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PostCategories]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostCategories](
	[PostID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Posts]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Posts](
	[PostID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[PostDate] [datetime] NOT NULL,
	[Post] [nvarchar](max) NOT NULL,
	[UserID] [nvarchar](128) NOT NULL,
	[StatusID] [int] NOT NULL,
	[PublishDate] [datetime] NULL,
	[ExpireDate] [datetime] NULL,
 CONSTRAINT [PK_Posts] PRIMARY KEY CLUSTERED 
(
	[PostID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PostTags]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostTags](
	[PostID] [int] NOT NULL,
	[TagID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Status]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[StatusID] [int] IDENTITY(1,1) NOT NULL,
	[StatusType] [nvarchar](25) NOT NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[StatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tags]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tags](
	[TagID] [int] IDENTITY(1,1) NOT NULL,
	[Tag] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Tags] PRIMARY KEY CLUSTERED 
(
	[TagID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201603072058201_InitialCreate', N'MSRM.Capstone.UI.Models.ApplicationDbContext', 0x1F8B0800000000000400DD5CDB6EE4B8117D0F907F10F49404DE962F99C1C468EFC2DBB63746C6174CDB8BBC0DD812BB4D8C446925CA6B23D82FDB877C527E214589BAF1A24BB7DCDD5E2CB09816C953C5E221592C16FDBFDFFF3BFDE125F0AD671C2724A467F6D1E4D0B63075438FD0D5999DB2E5779FEC1FBEFFF39FA6975EF062FD5CD43BE1F5A0254DCEEC27C6A253C749DC271CA0641210370E9370C9266E1838C80B9DE3C3C37F3847470E06081BB02C6BFA25A58C0438FB013F67217571C452E4DF841EF613F11D4AE619AA758B029C44C8C567F6CDFCCBCD6486A28485144F1EAF277913DB3AF7090275E6D85FDA16A234648881B2A78F099EB338A4AB79041F90FFF01A61A8B7447E8245274EABEA7DFB7378CCFBE3540D0B283705CD8281804727C2408EDC7C2D33DBA501C18497606AF6CA7B9D99F1CCBEF670F6E94BE883016481A7333FE695C1D2A588F324BAC56C52349CE4905731C0FD1AC6DF2675C403AB77BB839250C79343FEDF81354B7D96C6F88CE294C5C83FB0EED3854FDC7FE1D787F01BA66727478BE5C9A70F1F9177F2F1EFF8E443BDA7D057A8D7F8009FEEE330C231E8869765FF6DCB69B673E48665B35A9BDC2AC025981BB675835E3E63BA624F306B8E3FD9D61579C15EF14590EB9112984AD088C529FCBC4D7D1F2D7C5C963BAD32F9FF5BA41E7FF8388AD45BF44C56D9D04BF261E2C430AFBE603F2B4D9E48944FAFC6787F15D5AEE230E0BF9BFCCA4BBFCEC3347679674263950714AF306B6A37752AF2F6A234871A9FD605EAFE539B6BAAD25B5B9577689D995088D8F66C28F47D5BB9BD19771E45307819B5B8455A09A7DFB1261204B04BAA5891E8A82F892874EE8FBC265E0688F8232C8A3DA48053B2247180CB5EFE180205111DACF33D4A125813BC7FA2E4A94575F8E708AACFB19BC640D5394341F4E6D2EE9F80A9B769B0E033607BB2461B9A875FC32BE4B230BEA4BCD5C6789F43F75B98B24BEA5D20861F995B00F29F0F24E80F308A3AE7AE8B93E40AC88CBD59083E7701784DD9C9F16038BE4AEDDA2999F988047AAF445A4FBF16552BCF445F43F14E0CD5741E4A9BAA9FC315A1FD542DAA9A55CD6B74AA2AAA0D559583F5D354D4342B9A55E8D433AF359ACF978DD0F84E5F06BBFF5EDF669BB7692DA899710E2B24FE09531CC332E6DD23C6704CAB11E8B36EECC259C8868F0B7DF3BD2993F433F2D3B145AD351BB24560FCD990C1EEFF6CC8D484CFCFC4E35E498FA3505119E07BD5D79FB2BAE79CA4D9B6A743A39BDB16BE9D35C0345DCE93247449360B34413011C268EA0F3E9CD51DCFC87B23C744A0634074C2B73CF8027DB36552DDD10BEC6386AD73370F12CE50E2224F352374C81BA058B1A36A14AB62234DE5FEA6C804A6E3983742FC1094C04C2594A9D382509744C8EFB492D4B2E716C6FB5ECA904B2E70842917D869893EC2F5A110AE4029471A942E0B4D9D1AE3DA8968F05A4D63DEE5C256E3AE4428B6C2C90EDFD9C04BE1BFBD0931DB2DB60572B69BA48F02C6B0DE2E082ACE2A7D09201F5CF68DA0D289C94050E1526D85A04D8BED80A04D93BC3B82E647D4BEE32F9D57F78D9ECD83F2F6B7F55673ED809B0D7BEC193573DF13DA30688163959E170B5E885F98E670067A8AF359225C5D99221C7C8E59336453F9BB5A3FD469079149D4065811AD03545C082A40CA841AA05C11CB6BD54E781103608BB85B2BAC58FB25D81A0754ECFAC568ADA2F9FA542667AFD347D9B3920D0AC97B1D166A381A42C88B57B3E33D8C628ACBAA86E9E30B0FF1866B1D1383D162A00ECFD560A4A233A35BA9A066B795740ED910976C232B49EE93C14A456746B792E068B791344EC100B760231335B7F091265B11E928779BB26CEAE44953E2C3D43164574D6F501411BAAA655B892FD63C4FB59A7D371F9E7E14E4188E9B68B2904A6D4B492C8CD10A4BA5201A34BD2271C22E10430BC4E33C332F50AA69F756C3F25F88AC6F9FEA2016FB40519BFF3B6F61BAC66F6CB8AA472280AEA09B01776BB258BA8604FAE6164F81433E8A35E1FB59E8A701357B59E6D6F9255EBD7DFE4545983A92FE8A17A5984CF1759BF6EF353AEACC1873A44A4F66FDD13243986C5EF8A175AB9B7C53334A11AAAAA398C2573B1B3D934B337CC4649771F8807522BCCD0C13792A7500F16920462DD54101AB95F5476D66A3D4319B25FD11A594933AA4543440CB7A624943C97AC15A78068BEA6BF497A0A692D4D1D5D2FEC89AA4923AB4A6780D6C8DCE72597F544DDE491D5853DC1FBB4A429157D23DDEC38C0799CD36B1FCC0BBD92E66C0789B65719C4DB076AF5F07AA7D1E88256EEE1530F17D2F29653CF56D46A93CD8B119A50C18E635A8712DDE5C825AEFF2CD988DBBEEC632DF76D76FC61B46DC37A58772F293AB94D2CB13A074D29B8A5357F7631BE5189657B1ADC28CB0C5BF260C07135E6132FFC59FF904F305BDA870832859E284E5F91DF6F1E1D1B1F454677F9ECD3849E2F99A53ABE9ED4C73CCB690AA459F51EC3EA1584D9CD8E0694905AAC4A4AFA9875FCEECFF64AD4EB3F006FF57F6F9C0BA4E1E29F92585828738C5D66F6A22E838A9F6ED67AE3D7D18D1DFAAD7FFFE9A373DB0EE629831A7D6A164CB7546B8F95C62903679D30DB459FB11C5FB9D508D77095A546942ACFF0C6141D8284F100A2DFF12A097BF0E554DFBCC602344CD5382B1F04631A1E9A9C03A58C667021EFC64D93381619DD53F1B584735E39301428783C90F06FA2F4345CB1D6E359A83D13696A4CCCE9D09D71B655FEE7A6F52F2B2379AE86AEEF500B80DF2ABD760C63B4B4D1E6D77D4641E8F86BD4B6ABF79BAF1BE641857B91FBB4D2CDE662E71CB2DD11F2A85780F92DE34493CBB4F14DE36D74CC1DC3DCFB61C960EBC676413A95DBB4FFADD36D94C61DE3D27DBA0D4DE3DE3DAAEF6CF1D33ADF716BAF3445D35E7C87029A38B057725E2E6817338E12F422041EE51E6EF27F5995F6D59AB1D02AB2A66A1E6943359B0327114B94A8D76B1C3FA2A36FCD6CE8A3AED620D899A6DB2C5FADF2A5BD469976D487FDC450AB136015197D6DDB18EB5E544BDA794E1464F3A32D4BB7CD6D61BF6F794213C8A511AB3C77047FC7E12824731C99853674002B07ADD0B7B67ED2F30C2FE9D905505C1FF1E23C56E63D72CEB5CD365586CDE924645152942738319F2604B3D8F1959229741318F31670FC0B3B81DBFE95860EF9ADEA52C4A197419070BBF11F0E24E409BFC2CCBB9A9F3F42ECAFE96C9185D0035098FCDDFD11F53E27BA5DE579A989001827B1722A2CBC792F1C8EEEAB544BA0D694F2061BED2297AC041E403587247E7E819AFA31BD0EF335E21F7B58A009A40BA07A269F6E90541AB180589C0A8DAC34FE0B017BC7CFF7F6125F31688540000, N'6.1.3-40302')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'3', N'Disabled')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'1', N'SuperUser')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'2', N'User')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'fa8905d9-3c30-41a0-bc54-72a8c80cada8', N'm.chuong@yahoo.com', 0, N'AB6imYIGrjxkjRuLB6AlLelTjtmAAAwI0zJh0mhDXAfKDsQu3qtZ8eistONfmI8LTQ==', N'd577c464-76d1-4b78-9c76-ecb7246854ce', NULL, 0, 0, NULL, 1, 0, N'm.chuong@yahoo.com')
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (1, N'Events')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (2, N'.Net')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (3, N'UX/UI')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (4, N'Girl Tech')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (5, N'Programmer''s Life ')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (6, N'Community')
SET IDENTITY_INSERT [dbo].[Categories] OFF
INSERT [dbo].[PostCategories] ([PostID], [CategoryID]) VALUES (2, 3)
INSERT [dbo].[PostCategories] ([PostID], [CategoryID]) VALUES (2, 4)
INSERT [dbo].[PostCategories] ([PostID], [CategoryID]) VALUES (3, 1)
INSERT [dbo].[PostCategories] ([PostID], [CategoryID]) VALUES (3, 2)
SET IDENTITY_INSERT [dbo].[Posts] ON 

INSERT [dbo].[Posts] ([PostID], [Title], [PostDate], [Post], [UserID], [StatusID], [PublishDate], [ExpireDate]) VALUES (2, N'Hello, World!', CAST(N'2016-03-09 15:03:13.237' AS DateTime), N'Hello, you big, beautiful, cruel, insanely delicious world!', N'fa8905d9-3c30-41a0-bc54-72a8c80cada8', 1, NULL, NULL)
INSERT [dbo].[Posts] ([PostID], [Title], [PostDate], [Post], [UserID], [StatusID], [PublishDate], [ExpireDate]) VALUES (3, N'Good night, John Boy!', CAST(N'2016-03-09 15:56:19.583' AS DateTime), N'Good night, ya''ll! ANd you too, John Boy, you big, beautiful, cruel, insanely delicious little man!', N'fa8905d9-3c30-41a0-bc54-72a8c80cada8', 1, NULL, NULL)
INSERT [dbo].[Posts] ([PostID], [Title], [PostDate], [Post], [UserID], [StatusID], [PublishDate], [ExpireDate]) VALUES (12, N'Gimme Bacon!', CAST(N'1905-06-09 00:00:00.000' AS DateTime), N'<p>Bacon ipsum dolor amet nisi pork loin mollit aute ut pastrami frankfurter, exercitation shankle pig ut. Irure labore strip steak excepteur. Dolore short loin eiusmod laborum reprehenderit. Venison leberkas reprehenderit pork belly, minim jerky ut pork loin prosciutto frankfurter ea in non consectetur. Pariatur shoulder ullamco ribeye pork chop filet mignon eu cillum tempor anim pancetta tail short ribs jerky meatloaf.</p>', N'fa8905d9-3c30-41a0-bc54-72a8c80cada8', 1, CAST(N'1905-06-07 00:00:00.000' AS DateTime), CAST(N'1905-05-28 00:00:00.000' AS DateTime))
INSERT [dbo].[Posts] ([PostID], [Title], [PostDate], [Post], [UserID], [StatusID], [PublishDate], [ExpireDate]) VALUES (13, N'Luvs me some kohlrabi!', CAST(N'1905-06-09 00:00:00.000' AS DateTime), N'<p>Veggies es bonus vobis, proinde vos postulo essum magis kohlrabi welsh onion daikon amaranth tatsoi tomatillo melon azuki bean garlic.</p><p>Gumbo beet greens corn soko endive gumbo gourd. Parsley shallot courgette tatsoi pea sprouts fava bean collard greens dandelion okra wakame tomato. Dandelion cucumber earthnut pea peanut soko zucchini.</p><p>Turnip greens yarrow ricebean rutabaga endive cauliflower sea lettuce kohlrabi amaranth water spinach avocado daikon napa cabbage asparagus winter purslane kale. Celery potato scallion desert raisin horseradish spinach carrot soko. Lotus root water spinach fennel kombu maize bamboo shoot green bean swiss chard seakale pumpkin onion chickpea gram corn pea. Brussels sprout coriander water chestnut gourd swiss chard wakame kohlrabi beetroot carrot watercress. Corn amaranth salsify bunya nuts nori azuki bean chickweed potato bell pepper artichoke.</p>', N'fa8905d9-3c30-41a0-bc54-72a8c80cada8', 1, CAST(N'1905-06-07 00:00:00.000' AS DateTime), CAST(N'1905-05-28 00:00:00.000' AS DateTime))
INSERT [dbo].[Posts] ([PostID], [Title], [PostDate], [Post], [UserID], [StatusID], [PublishDate], [ExpireDate]) VALUES (14, N'Salami is my favorite language.', CAST(N'1905-06-09 00:00:00.000' AS DateTime), N'<p>Salami do enim rump dolore voluptate short ribs. Tempor labore irure prosciutto bacon. Pork meatloaf consequat, capicola ham hock t-bone shankle frankfurter picanha aliqua kielbasa jerky cupidatat cillum in. Fatback sausage cow beef ribs velit, pork loin ex. Anim frankfurter leberkas, t-bone turkey ham hock venison ham hamburger labore sint tempor ut. Pariatur swine consequat, pork belly jerky commodo dolore id ham hock meatball. Laboris et pariatur duis pig.</p>', N'fa8905d9-3c30-41a0-bc54-72a8c80cada8', 1, CAST(N'1905-06-07 00:00:00.000' AS DateTime), CAST(N'1905-05-28 00:00:00.000' AS DateTime))
INSERT [dbo].[Posts] ([PostID], [Title], [PostDate], [Post], [UserID], [StatusID], [PublishDate], [ExpireDate]) VALUES (16, N'Sausage and Tenderloin plugins now available', CAST(N'1905-06-09 00:00:00.000' AS DateTime), N'<p>Do fugiat id, adipisicing sausage nostrud flank. Tenderloin spare ribs excepteur cow, duis chuck pork chop. Venison mollit adipisicing tenderloin voluptate dolor chicken brisket laborum nulla. Nostrud picanha sint, short ribs in venison meatball labore chuck tenderloin ham hock lorem meatloaf tail andouille. Tail t-bone in cupim tenderloin sunt swine lorem ham hock pancetta jowl. Ex excepteur consequat, velit pork loin pork belly et jerky cupim tempor exercitation voluptate shoulder ut. Deserunt laborum ribeye turducken kevin chuck pariatur.</p>', N'fa8905d9-3c30-41a0-bc54-72a8c80cada8', 1, CAST(N'1905-06-07 00:00:00.000' AS DateTime), CAST(N'1905-05-28 00:00:00.000' AS DateTime))
INSERT [dbo].[Posts] ([PostID], [Title], [PostDate], [Post], [UserID], [StatusID], [PublishDate], [ExpireDate]) VALUES (18, N'It''s time for the Pancetta Mashup at Pork Bellies.', CAST(N'1905-06-09 00:00:00.000' AS DateTime), N'<p>Labore pancetta aliqua pork belly frankfurter alcatra. Duis spare ribs fatback, ball tip nulla adipisicing flank salami chuck. Veniam landjaeger ad doner alcatra. Bacon ad beef ribs laborum adipisicing, veniam proident alcatra sint. Est magna turducken ut pork chop dolore alcatra cupim, aliqua meatball. Ribeye fugiat chuck deserunt excepteur. Short ribs sint doner, reprehenderit cupidatat bresaola tail bacon fugiat pork loin.</p><p>Meatloaf ex pastrami drumstick leberkas, beef ribs andouille voluptate chuck consectetur filet mignon corned beef turducken. Dolore cupidatat nostrud anim pariatur. Shankle velit frankfurter andouille fugiat, short ribs in ex landjaeger minim strip steak fatback picanha bacon. Landjaeger deserunt laborum meatball bresaola spare ribs aute shank tongue elit incididunt ut officia. Venison culpa strip steak, bacon sausage andouille veniam magna aute. Strip steak pork loin pork ut, ex filet mignon pastrami chicken ad.</p>', N'fa8905d9-3c30-41a0-bc54-72a8c80cada8', 1, CAST(N'1905-06-07 00:00:00.000' AS DateTime), CAST(N'1905-05-28 00:00:00.000' AS DateTime))
INSERT [dbo].[Posts] ([PostID], [Title], [PostDate], [Post], [UserID], [StatusID], [PublishDate], [ExpireDate]) VALUES (19, N'Dulse de Parsnips vs MVC.', CAST(N'1905-06-09 00:00:00.000' AS DateTime), N'<p>Soko radicchio bunya nuts gram dulse silver beet parsnip napa cabbage lotus root sea lettuce brussels sprout cabbage. Catsear cauliflower garbanzo yarrow salsify chicory garlic bell pepper napa cabbage lettuce tomato kale arugula melon sierra leone bologi rutabaga tigernut. Sea lettuce gumbo grape kale kombu cauliflower salsify kohlrabi okra sea lettuce broccoli celery lotus root carrot winter purslane turnip greens garlic. JÃ­cama garlic courgette coriander radicchio plantain scallion cauliflower fava bean desert raisin spring onion chicory bunya nuts. Sea lettuce water spinach gram fava bean leek dandelion silver beet eggplant bush tomato.</p>', N'fa8905d9-3c30-41a0-bc54-72a8c80cada8', 1, CAST(N'1905-06-07 00:00:00.000' AS DateTime), CAST(N'1905-05-28 00:00:00.000' AS DateTime))
INSERT [dbo].[Posts] ([PostID], [Title], [PostDate], [Post], [UserID], [StatusID], [PublishDate], [ExpireDate]) VALUES (20, N'Broccoli and beet greens are the developer''s secret weapon.', CAST(N'1905-06-09 00:00:00.000' AS DateTime), N'<p>Nori grape silver beet broccoli kombu beet greens fava bean potato quandong celery. Bunya nuts black-eyed pea prairie turnip leek lentil turnip greens parsnip. Sea lettuce lettuce water chestnut eggplant winter purslane fennel azuki bean earthnut pea sierra leone bologi leek soko chicory celtuce parsley jícama salsify.</p><p>Celery quandong swiss chard chicory earthnut pea potato. Salsify taro catsear garlic gram celery bitterleaf wattle seed collard greens nori. Grape wattle seed kombu beetroot horseradish carrot squash brussels sprout chard.</p>', N'fa8905d9-3c30-41a0-bc54-72a8c80cada8', 1, CAST(N'1905-06-07 00:00:00.000' AS DateTime), CAST(N'1905-05-28 00:00:00.000' AS DateTime))
INSERT [dbo].[Posts] ([PostID], [Title], [PostDate], [Post], [UserID], [StatusID], [PublishDate], [ExpireDate]) VALUES (21, N'The Pea pattern is more flexible than the Avocado method.', CAST(N'1905-06-09 00:00:00.000' AS DateTime), N'<p>Pea horseradish azuki bean lettuce avocado asparagus okra. Kohlrabi radish okra azuki bean corn fava bean mustard tigernut jícama green bean celtuce collard greens avocado quandong fennel gumbo black-eyed pea. Grape silver beet watercress potato tigernut corn groundnut. Chickweed okra pea winter purslane coriander yarrow sweet pepper radish garlic brussels sprout groundnut summer purslane earthnut pea tomato spring onion azuki bean gourd. Gumbo kakadu plum komatsuna black-eyed pea green bean zucchini gourd winter purslane silver beet rock melon radish asparagus spinach.</p><p>Beetroot water spinach okra water chestnut ricebean pea catsear courgette summer purslane. Water spinach arugula pea tatsoi aubergine spring onion bush tomato kale radicchio turnip chicory salsify pea sprouts fava bean. Dandelion zucchini burdock yarrow chickpea dandelion sorrel courgette turnip greens tigernut soybean radish artichoke wattle seed endive groundnut broccoli arugula.</p>', N'fa8905d9-3c30-41a0-bc54-72a8c80cada8', 1, CAST(N'1905-06-07 00:00:00.000' AS DateTime), CAST(N'1905-05-28 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Posts] OFF
INSERT [dbo].[PostTags] ([PostID], [TagID]) VALUES (2, 2)
INSERT [dbo].[PostTags] ([PostID], [TagID]) VALUES (2, 4)
INSERT [dbo].[PostTags] ([PostID], [TagID]) VALUES (2, 6)
INSERT [dbo].[PostTags] ([PostID], [TagID]) VALUES (3, 1)
INSERT [dbo].[PostTags] ([PostID], [TagID]) VALUES (3, 3)
INSERT [dbo].[PostTags] ([PostID], [TagID]) VALUES (3, 5)
SET IDENTITY_INSERT [dbo].[Status] ON 

INSERT [dbo].[Status] ([StatusID], [StatusType]) VALUES (1, N'Draft')
INSERT [dbo].[Status] ([StatusID], [StatusType]) VALUES (2, N'Review')
INSERT [dbo].[Status] ([StatusID], [StatusType]) VALUES (3, N'Decline')
INSERT [dbo].[Status] ([StatusID], [StatusType]) VALUES (4, N'Published')
SET IDENTITY_INSERT [dbo].[Status] OFF
SET IDENTITY_INSERT [dbo].[Tags] ON 

INSERT [dbo].[Tags] ([TagID], [Tag]) VALUES (1, N'C#')
INSERT [dbo].[Tags] ([TagID], [Tag]) VALUES (2, N'MVC')
INSERT [dbo].[Tags] ([TagID], [Tag]) VALUES (3, N'jQuery')
INSERT [dbo].[Tags] ([TagID], [Tag]) VALUES (4, N'CSS')
INSERT [dbo].[Tags] ([TagID], [Tag]) VALUES (5, N'Bootstrap')
INSERT [dbo].[Tags] ([TagID], [Tag]) VALUES (6, N'AngularJS')
SET IDENTITY_INSERT [dbo].[Tags] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 3/10/2016 9:29:26 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 3/10/2016 9:29:26 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 3/10/2016 9:29:26 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 3/10/2016 9:29:26 AM ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 3/10/2016 9:29:26 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 3/10/2016 9:29:26 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[PostCategories]  WITH CHECK ADD  CONSTRAINT [FK_PostCategories_Categories] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Categories] ([CategoryID])
GO
ALTER TABLE [dbo].[PostCategories] CHECK CONSTRAINT [FK_PostCategories_Categories]
GO
ALTER TABLE [dbo].[PostCategories]  WITH CHECK ADD  CONSTRAINT [FK_PostCategories_Posts] FOREIGN KEY([PostID])
REFERENCES [dbo].[Posts] ([PostID])
GO
ALTER TABLE [dbo].[PostCategories] CHECK CONSTRAINT [FK_PostCategories_Posts]
GO
ALTER TABLE [dbo].[Posts]  WITH CHECK ADD  CONSTRAINT [FK_Posts_AspNetUsers] FOREIGN KEY([UserID])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Posts] CHECK CONSTRAINT [FK_Posts_AspNetUsers]
GO
ALTER TABLE [dbo].[Posts]  WITH CHECK ADD  CONSTRAINT [FK_Posts_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([StatusID])
GO
ALTER TABLE [dbo].[Posts] CHECK CONSTRAINT [FK_Posts_Status]
GO
ALTER TABLE [dbo].[PostTags]  WITH CHECK ADD  CONSTRAINT [FK_PostTags_Posts] FOREIGN KEY([PostID])
REFERENCES [dbo].[Posts] ([PostID])
GO
ALTER TABLE [dbo].[PostTags] CHECK CONSTRAINT [FK_PostTags_Posts]
GO
ALTER TABLE [dbo].[PostTags]  WITH CHECK ADD  CONSTRAINT [FK_PostTags_Tags] FOREIGN KEY([TagID])
REFERENCES [dbo].[Tags] ([TagID])
GO
ALTER TABLE [dbo].[PostTags] CHECK CONSTRAINT [FK_PostTags_Tags]
GO
/****** Object:  StoredProcedure [dbo].[GetAllPosts]    Script Date: 3/10/2016 9:29:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllPosts]
AS

-- Get all information about a post
SELECT *
FROM Posts


GO
USE [master]
GO
ALTER DATABASE [Capstone] SET  READ_WRITE 
GO
