namespace SGI.Dealership.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addcustomeremailfield : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InformationRequests", "CustomerEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.InformationRequests", "CustomerEmail");
        }
    }
}
