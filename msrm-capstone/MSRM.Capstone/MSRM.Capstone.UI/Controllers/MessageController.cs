﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MSRM.Capstone.UI.Controllers
{
    public class MessageController : Controller
    {
        [Authorize(Roles = "SuperUser")]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SendMessage()
        {
            return View();
        }

    }
}