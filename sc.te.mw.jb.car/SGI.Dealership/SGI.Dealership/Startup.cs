﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SGI.Dealership.Startup))]
namespace SGI.Dealership
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
