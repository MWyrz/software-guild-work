﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL;
using Flooring.Models;

namespace Flooring.UI
{
    public class RemoveOrderWorkflow
    {
        public void Execute()
        {
            string config = ConfigurationSettings.AppSettings.Get("production");
            string date = UserPrompts.GetDateFromUser();
            OrderManager manager = new OrderManager(config);
            bool foundOrder = false;

            var response = manager.DisplayOrders(date);
            Order orderToRemove = new Order();
            if (response.Success)
            {
                int number = UserPrompts.GetOrderNumberFromUser();
                foreach (var order in response.Data)
                {
                    if (order.OrderNumber == number)
                    {
                        manager.DisplayOrder(order);
                        orderToRemove = order;
                        Console.WriteLine();
                        foundOrder = true;
                    }
                }
                if (foundOrder)
                {
                    if (UserPrompts.GetUserConfirmation())
                    {
                        manager.RemoveOrder(date, orderToRemove);
                        Console.WriteLine("\nOrder number " + orderToRemove.OrderNumber + " removed.\n");
                    }
                    else
                    {
                        Console.WriteLine("\nReturning to Main Menu.\n");
                    }
                }
                else
                {
                    Console.WriteLine("There is no order with number {0} on {1}", number, date);
                    manager.AddError(DateTime.Now + ": User entered an invalid order number(" + number + ") for date(" + date + ").");
                }
            }
            else
            {
                Console.WriteLine(response.Message);
            }
            UserPrompts.PressAnyKeyToContinue();

        }
    }
}
