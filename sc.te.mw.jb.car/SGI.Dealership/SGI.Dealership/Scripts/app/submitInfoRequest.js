﻿$(document).ready(function() {
    $("#btnShowInfoRequest").click(function() {
        $("#infoRequestModal").modal("show");
        $("#CustomerName").val("");
        $("#CustomerPhone").val("");
        $("#CustomerAdditionalInformation").val("");
    });

    $("#btnSubmitInfoRequest").click(function() {
        var infoRequest = {};

        infoRequest.RequestStatusId = 1;
        infoRequest.VehicleId = $("#VehicleId").val();
        infoRequest.DateSubmitted = moment().format("MM DD YYYY");
        infoRequest.LastContactDate = moment().format("MM DD YYYY");
        infoRequest.CustomerName = $("#CustomerName").val();
        infoRequest.CustomerPhone = $("#CustomerPhone").val();
        if ($("#preferPhone").is(":checked")) {
            infoRequest.CustomerPreferredContactMethod = $("#preferPhone").val();
        }
        if ($("#preferEmail").is(":checked")) {
            infoRequest.CustomerPreferredContactMethod = $("#preferEmail").val();
        }
        if ($("#morning").is(":checked")) {
            infoRequest.CustomerBestTimeToContact = $("#morning").val();
        }
        if ($("#afternoon").is(":checked")) {
            infoRequest.CustomerBestTimeToContact = $("#afternoon").val();
        }
        if ($("#evening").is(":checked")) {
            infoRequest.CustomerBestTimeToContact = $("#evening").val();
        }
        if ($("#lessThan3").is(":checked")) {
            infoRequest.CustomerTimeFrameToBuy = $("#lessThan3").val();
        }
        if ($("#moreThan3").is(":checked")) {
            infoRequest.CustomerTimeFrameToBuy = $("#moreThan3").val();
        }
        infoRequest.CustomerAdditionalInformation = $("#CustomerAdditionalInformation").val();

        $.post(infoRequestUri, infoRequest)
            .done(function() {
                $("#infoRequestModal").modal("hide");
            })
            .fail(function (jqXhr, status, err) {
                $("<div class=\"col-xs-offset-1 col-xs-10 alert alert-danger\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a>" + err + "</div>").appendTo("#errorMsg");
            });
    });
});