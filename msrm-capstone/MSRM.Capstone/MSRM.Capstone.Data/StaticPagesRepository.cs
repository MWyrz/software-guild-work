﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using Dapper;
using MSRM.Capstone.Data.Settings;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.Data
{
    public class StaticPagesRepository
    {
        public int CreateStaticPage(StaticPage newStaticPage)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@StaticPageId", DbType.Int32, direction: ParameterDirection.Output);
                parameters.Add("@StaticPageTitle", newStaticPage.StaticPageTitle);
                parameters.Add("@StaticPageContent", newStaticPage.StaticPageContent);

                cn.Execute("CreateStaticPage", parameters, commandType: CommandType.StoredProcedure);
                int staticPageId = parameters.Get<int>("StaticPageId");
                newStaticPage.StaticPageId = staticPageId;

                return staticPageId;
            }
        }

        public List<StaticPage> GetAllStaticPages()
        {
            var staticPages = new List<StaticPage>();

            using (var cn = new SqlConnection(Config.ConnectionString))
            {
                var cmd = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "GetAllStaticPages",
                    Connection = cn
                };
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        staticPages.Add(PopulateFromDataReader(dr));
                    }
                }
            }
            return staticPages;
        }


        public List<StaticPage> GetPageHeaders()
        {
            var pageHeaders = new List<StaticPage>();

            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT p.StaticPageID, p.StaticPageTitle FROM Pages p";
                cmd.Connection = cn;
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.HasRows && dr.Read())
                    {
                        pageHeaders.Add(PageHeadersDataReader(dr));
                    }
                }
            }
            return pageHeaders;
        }

        private StaticPage PageHeadersDataReader(SqlDataReader dr)
        {
            StaticPage pageHeaders = new StaticPage();
            pageHeaders.StaticPageId = (int)dr["StaticPageId"];
            pageHeaders.StaticPageTitle = dr["StaticPageTitle"].ToString();

            return pageHeaders;
        }


        public StaticPage GetStaticPageById(int staticPageId)
        {
            var staticPage = new StaticPage();

            using (var cn = new SqlConnection(Config.ConnectionString))
            {
                var cmd = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "GetPageById",
                    Connection = cn
                };
                cmd.Parameters.AddWithValue("@StaticPageId", staticPageId);
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    staticPage = PopulateFromDataReader(dr);
                }
            }
            return staticPage;
        }


        private StaticPage PopulateFromDataReader(SqlDataReader dr)
        {
            var staticPage = new StaticPage();
            while (dr.HasRows && dr.Read())
            {
                staticPage.StaticPageId = (int) dr["StaticPageId"];
                staticPage.StaticPageTitle = dr["StaticPageTitle"].ToString();
                staticPage.StaticPageContent = dr["StaticPageContent"].ToString();
            }

            return staticPage;
        }

        public void DeletePage(int pageId)
        {
            var p = new DynamicParameters();
            p.Add("StaticPageId", pageId);

            using (SqlConnection cn = new SqlConnection(Settings.Config.ConnectionString))
            {
                cn.Execute("DeletePage", p,
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}
