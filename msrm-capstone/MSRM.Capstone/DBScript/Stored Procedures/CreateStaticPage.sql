CREATE PROCEDURE CreateStaticPage
(
	@StaticPageId int OUTPUT,
	@StaticPageContent nvarchar(max),
	@StaticPageTitle nvarchar(255)
) AS

INSERT INTO Pages (StaticPageContent, StaticPageTitle) 
VALUES (@StaticPageContent, @StaticPageTitle)
SET	@StaticPageId = SCOPE_IDENTITY();

GO