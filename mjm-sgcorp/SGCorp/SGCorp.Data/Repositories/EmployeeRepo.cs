﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Linq;
using SGCorp.Models.Models;

namespace SGCorp.Data.Repositories
{
    public class EmployeeRepo
    {
        private string _fileName = HostingEnvironment.MapPath("/App_Data/Employees.xml");

        public List<Employee> GetAll()
        {
            var xElement = XDocument.Load(_fileName).Root;
            if (xElement != null)
            {
                var employeeList = (from e in xElement.Elements("Employee")
                                    select new Employee()
                                    {
                                        EmpId = (int)e.Element("EmpId"),
                                        LastName = (string)e.Element("LastName"),
                                        FirstName = (string)e.Element("FirstName"),
                                        HireDate = (DateTime)e.Element("HireDate"),
                                        Entries = (from f in e.Elements("Entry")
                                                    select new TimeEntry()
                                                    {
                                                        EntryId = (int)f.Element("EntryId"),
                                                        HourWorked = (int)f.Element("HourWorked"),
                                                        TodaysDate = (DateTime)f.Element("TodaysDate"),
                                                        EmpId = (int)f.Element("EmpId")
                                                    }).ToList()

                                    }).ToList();

                return employeeList;
            }
            return null;
        }

        public void AddTime(TimeEntry timeEntry)
        {
            var employees = GetAll();
           
                foreach (var emp in employees)
                {
                    if (emp.EmpId == timeEntry.EmpId)
                    {
                        if (emp.Entries.Count != 0)
                        {
                            timeEntry.EntryId = emp.Entries.Max(e => e.EntryId) + 1;
                        }
                        else
                        {
                            timeEntry.EntryId = 1;
                        }
                        emp.Entries.Add(timeEntry);
                    }
                }
               
            OverWrite(employees);
        }

        public Employee GetEmpById(int id)
        {
            var empList = GetAll();

            var emp = empList.FirstOrDefault(a => a.EmpId == id);

            return emp;
        }

        public void DeleteTime(TimeEntry entry)
        {
            var empList = GetAll();
            var timeList = empList.FirstOrDefault(a => a.EmpId == entry.EmpId)?.Entries;

            timeList?.Remove(timeList.FirstOrDefault(a => a.EntryId == entry.EntryId));

            OverWrite(empList);
        }
    

        private void OverWrite(List<Employee> employees)
        {
            if (File.Exists(_fileName))
            {
                File.Delete(_fileName);
            }

            using (var writer = XmlWriter.Create(_fileName))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Employees");


                foreach (var employee in employees)
                {
                    writer.WriteStartElement("Employee");
                    writer.WriteElementString("EmpId", employee.EmpId.ToString());
                    writer.WriteElementString("LastName", employee.LastName);
                    writer.WriteElementString("FirstName", employee.FirstName);
                    writer.WriteElementString("HireDate", employee.HireDate.ToShortDateString());
                   
                    foreach (var entry in employee.Entries)
                    {
                        writer.WriteStartElement("Entry");
                        writer.WriteElementString("EntryId", entry.EntryId.ToString());
                        writer.WriteElementString("HourWorked", entry.HourWorked.ToString());
                        writer.WriteElementString("TodaysDate", entry.TodaysDate.ToShortDateString());
                        writer.WriteElementString("EmpId", entry.EmpId.ToString());
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
    }
}
