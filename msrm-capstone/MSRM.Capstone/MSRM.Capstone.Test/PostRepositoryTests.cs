﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSRM.Capstone.Data;
using MSRM.Capstone.Model;
using NUnit.Framework;

namespace MSRM.Capstone.Test
{
    [TestFixture]
    public class PostRepositoryTests
    {
        private PostRepository _postRepo;
        private Post _testPost;

        [SetUp]
        public void TestSetUp()
        {
            Utilities.RebuildTestDb();
            _postRepo = new PostRepository();
            _testPost = new Post();      
        }

        [Test]
        public void CanGetAllPosts()
        {
            List<Post> actual = _postRepo.GetAllPosts();

            Assert.AreEqual(18, actual.Count);
        }

        [Test]
        public void CreatePost()
        {
            Post newPost = new Post
            {
                UserId = "fa8905d9-3c30-41a0-bc54-72a8c80cada8",
                StatusId = 1,
                PostDate = DateTime.Today,
                Posting = "New POST!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",
                Title = "Better work!"
            };

            int newId = _postRepo.Create(newPost);

            Assert.AreEqual(51, newId);
        }

        [Test]
        public void GetAllStatusTest()
        {
            var repo = new StatusRepository();
            var statuses = repo.GetAllStatus();

            Assert.AreEqual(4, statuses.Count);
        }

    }

}
