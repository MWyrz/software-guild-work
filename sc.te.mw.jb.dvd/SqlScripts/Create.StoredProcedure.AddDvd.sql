USE [TestDvdLibrary]
GO
/****** Object:  StoredProcedure [dbo].[AddDvd]    Script Date: 2/26/2016 11:17:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[AddDvd]
(
    @StudioId int,
    @Title nvarchar(50),
    @ReleaseDate Date,
    @MPAARating nvarchar(50),
    @ActorId int,
    @DirectorId int,
    @DvdId int output
) AS
BEGIN

INSERT INTO Dvd(StudioId, Title, ReleaseDate, MPAARating)
VALUES (@StudioId, @Title, @ReleaseDate, @MPAARating)

SET @DvdId = SCOPE_IDENTITY();

INSERT INTO DvdActors(DvdId,ActorId)
VALUES (@DvdId, @ActorId)

INSERT INTO DvdDirectors(DvdId, DirectorId)
VALUES (@DvdId, @DirectorId)

END


/****** Object:  StoredProcedure [dbo].[GetDvdInfo]    Script Date: 2/26/2016 10:11:05 AM ******/
SET ANSI_NULLS ON
