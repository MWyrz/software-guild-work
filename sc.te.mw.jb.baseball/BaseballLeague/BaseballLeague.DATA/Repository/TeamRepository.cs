﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseballLeague.CONTRACTS.Repository;
using BaseballLeague.DATA.Config;
using BaseballLeague.MODELS;

namespace BaseballLeague.DATA.Repository
{
    public class TeamRepository : ITeamRepository
    {
        private string _connectionString = Settings.ConnectionString;

        public List<Team> LoadAll()
        {
            List<Team> teams = new List<Team>();
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Teams";
                cmd.Connection = cn;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Team newTeam = new Team();
                        newTeam.TeamId = (int)dr["TeamId"];
                        newTeam.LeagueId = (int)dr["LeagueId"];
                        newTeam.TeamName = dr["TeamName"].ToString();
                        teams.Add(newTeam);
                    }
                }
            }


            return teams;
        }

        public Team Load(int teamId)
        {
            throw new NotImplementedException();
        }

        public Team Add(Team team)
        {
            throw new NotImplementedException();
        }

        public Team Edit(int teamId, Team team)
        {
            throw new NotImplementedException();
        }

        public int Remove(int teamId)
        {
            throw new NotImplementedException();
        }
    }
}
