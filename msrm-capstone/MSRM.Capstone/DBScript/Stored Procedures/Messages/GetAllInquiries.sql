USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[GetAllInquiries]    Script Date: 3/21/2016 12:56:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[GetAllInquiries]
AS

Select i.InquiryID AS MessageId, i.MessageStatus AS isRead, i.FirstName, i.LastName, i.InquiryDate AS [Date], i.Inquiry AS MessageBody, i.EmailAddress
From Inquiries i


GO

