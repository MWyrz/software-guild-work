USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[DeleteTag]    Script Date: 3/10/2016 10:25:12 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[DeleteTag]
(
	@Id int
) As

Delete Tags
Where TagID = @Id


GO

