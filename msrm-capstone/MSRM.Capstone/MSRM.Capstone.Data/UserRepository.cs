﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MSRM.Capstone.Data.Settings;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.Data
{
    public class UserRepository
    {
        public List<User> GetAllUsers()
        {
            var userList = new List<User>();
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                userList = cn.Query<User>("SELECT u.UserName, u.Email AS UserEmail, r.RoleId FROM AspNetUsers u JOIN AspNetUserRoles r ON u.Id = r.UserId").ToList();
            }
            return userList;
        }

        public void AlterUserInfo(User user)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var c = new DynamicParameters();
                c.Add("Email", user.UserEmail);
                c.Add("UserName", user.UserName);

                cn.Execute("AlterUserInfo", c, commandType: CommandType.StoredProcedure);
                ChangeUserRole(user.UserEmail, user.RoleId);
            }
        }

        public void ChangeUserRole(string email, int role)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var c = new DynamicParameters();
                c.Add("Email", email);
                c.Add("Role", role);

                cn.Execute("RegisterUserRole", c, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
