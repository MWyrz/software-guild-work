﻿$(document).ready(function () {
    $("#btnShowAddVehicle").click(function () {
        $("#addVehicleModal").modal("show");
        $("#Make").val("");
        $("#Model").val("");
        $("#Year").val("");
        $("#Condition").val("");
        $("#Mileage").val("");
        $("#Price").val("");
        $("#AdTitle").val("");
        $("#Description").val("");
    });

    $("#btnSaveVehicle").click(function () {
        var vehicle = {};

        vehicle.Make = $("#Make").val();
        vehicle.Model = $("#Model").val();
        vehicle.Year = $("#Year").val();
        vehicle.Condition = $("#Condition").val();
        vehicle.Mileage = $("#Mileage").val();
        vehicle.Price = $("#Price").val();
        vehicle.AdTitle = $("#AdTitle").val();
        vehicle.Description = $("#Description").val();

        $.post(vehicleUri, vehicle)
            .done(function () {
                $("#addVehicleModal").modal("hide");
                window.location.href = "/Home/Index";
            })
            .fail(function (jqXhr, status, err) {
                $("#addVehicleModal").modal("hide");
                $("#errorMsg").appendTo("<div class=\"col-xs-offset-1 col-xs-10 alert alert-danger\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a>" + err + "</div>");
                window.location.href = "/Home/Index";
            });
    });
});