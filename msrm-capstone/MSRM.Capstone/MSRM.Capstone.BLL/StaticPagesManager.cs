﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSRM.Capstone.Data;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.BLL
{
    public class StaticPagesManager
    {
        private readonly StaticPagesRepository _repo = new StaticPagesRepository();

        public Response<StaticPage> CreateStaticPage(StaticPage newStaticPage)
        {
            var response = new Response<StaticPage>();
            try
            {
                response.Success = true;
                response.Message = "A new static page has been created";
                int newId = _repo.CreateStaticPage(newStaticPage);
                newStaticPage.StaticPageId = newId;
                response.Data = newStaticPage;
            }
            catch (Exception ex)
            {
                var exMsg = ex.Message;
                response.Success = false;
                response.Message = "Failed to create a new static page.";
            }
            return response;
        }


        public Response<List<StaticPage>> GetAllStaticPages()
        {
            var response = new Response<List<StaticPage>>();
            try
            {
                response.Success = true;
                response.Message = "All static pages retrieved from the database.";
                response.Data = _repo.GetAllStaticPages();
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Failed to retrieve all static pages from the database.";
                response.Data = new List<StaticPage>();
            }
            return response;
        }


        public Response<List<StaticPage>> GetPageHeaders()
        {
            var response = new Response<List<StaticPage>>();
            try
            {
                response.Success = true;
                response.Message = "All pages header retrieved from the database.";
                response.Data = _repo.GetPageHeaders();
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Failed to retrieve all page headers from the database.";
                response.Data = new List<StaticPage>();
            }
            return response;
        }


        public Response<StaticPage> GetStaticPageById(int staticPageId)
        {
            var response = new Response<StaticPage>();
            try
            {
                response.Success = true;
                response.Message = "The static page has been retrieved.";
                response.Data = _repo.GetStaticPageById(staticPageId);
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "The static page could not be retrieved.";
                response.Data = new StaticPage();
            }
            return response;
        }

        public Response<StaticPage> DeletePage(int pageId)
        {
            var response = new Response<StaticPage>();

            try
            {
                response.Success = true;
                response.Message = "Static Page successfully deleted.";
                _repo.DeletePage(pageId);
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Failed to delete static page.";
            }
            return response;
        }
    }
}
