﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            MainMenu menu = new MainMenu();
            while (true)
            {
                Console.Clear();
                menu.Execute();
            }
        }
    }
}
