﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGCorp.Models.Models
{
    public class Employee
    {
        public int EmpId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime HireDate { get; set; }
        public int TotalHours
        {
            get
            {
                int total = 0;

                foreach (var entry in Entries)
                {
                    total += entry.HourWorked;
                }
                return total;
            }
        }

        public List<TimeEntry> Entries { get; set; }
    }
}
