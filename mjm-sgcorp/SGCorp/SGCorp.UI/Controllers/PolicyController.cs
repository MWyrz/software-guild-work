﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGCorp.BLL;
using SGCorp.Models.Models;

namespace SGCorp.UI.Controllers
{
    public class PolicyController : Controller
    {
        //public ActionResult ViewPolicies(Category category)
        //{
        // return View(category.Policies);
        //}

        public ActionResult ViewPolicies()
        {
            PolicyManager manager = new PolicyManager();
            var categories = manager.GetAllCategories();

            return View(categories);
        }

        public ActionResult ManagePolicies()
        {
            PolicyManager manager = new PolicyManager();
            var categories = manager.GetAllCategories();

            return View(categories);
        }

        public ActionResult ManageCategories()
        {
            PolicyManager manager = new PolicyManager();
            var categories = manager.GetAllCategories();

            return View(categories);
        }

        public ActionResult ViewPolicyTable(int id)
        {
            PolicyManager manager = new PolicyManager();
            var category = manager.GetByID(id);
            return View(category);
        }

        public ActionResult DeletePolicy(Policy policy)
        {
            PolicyManager manager = new PolicyManager();
            manager.DeletePolicy(policy);

            return RedirectToAction("ManagePolicies");
        }

        public ActionResult DeleteCategory(Category category)
        {
            PolicyManager manager = new PolicyManager();
            manager.DeleteCategory(category.CategoryId);

            return RedirectToAction("ManageCategories");
        }


        public ActionResult EditPolicy(Policy policy)
        {
            return View(policy);
        }

        [HttpPost]
        public ActionResult SaveEditPolicy(Policy policy)
        {

            var manager = new PolicyManager();

            manager.EditPolicy(policy);
            return RedirectToAction("ManagePolicies");
        }

        public ActionResult AddPolicy()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddPolicy(Policy policy)
        {
            var manager = new PolicyManager();
            if (ModelState.IsValid)
            {
                manager.AddPolicy(policy);
                return RedirectToAction("ManagePolicies");
            }
            return View(policy);
        }

        //public ActionResult ManageCategories()
        //{

            //    return View();
            //}

        public ActionResult ViewSinglePolicy(string descrpition)
        {
           return View(descrpition);
        }
    }
}