﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGCorp.BLL;
using SGCorp.Models.Models;

namespace SGCorp.UI.Controllers
{
    public class TimeEntryController : Controller
    {
        public ActionResult SubmitTime()
        {
            EmployeeManager manager = new EmployeeManager();
            ViewBag.Employees = manager.GetAllEmployees();
            return View();
        }

        public ActionResult ViewTimeSheet()
        {
            EmployeeManager manager = new EmployeeManager();
            var employees = manager.GetAllEmployees();

            return View(employees);
        }


        [HttpPost]
        public ActionResult SubmitTime(TimeEntry time )
        {
            EmployeeManager manager = new EmployeeManager();
            manager.AddTime(time);

            return RedirectToAction("ViewTimeSheet");
        }

        public ActionResult ViewEmployeeTime(int id)
        {
            EmployeeManager manager = new EmployeeManager();
            var emp = manager.GetEmpByID(id);
            return View(emp);
        }
        public ActionResult DeleteTime(TimeEntry entry)
        {
            EmployeeManager manager = new EmployeeManager();
            manager.DeleteTime(entry);

            return RedirectToAction("ViewEmployeeTime", new {id = entry.EmpId});
        }
    }
}