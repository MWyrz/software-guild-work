﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGCorp.Data.Repositories;
using SGCorp.Models.Models;

namespace SGCorp.BLL
{
    public class EmployeeManager
    {
        public List<Employee> GetAllEmployees()
        {
            EmployeeRepo repo = new EmployeeRepo();
            return repo.GetAll();
        }

        public void AddTime(TimeEntry timeEntry)
        {
            EmployeeRepo repo = new EmployeeRepo();
            repo.AddTime(timeEntry);
        }

        public Employee GetEmpByID(int id)
        {
            EmployeeRepo repo = new EmployeeRepo();

            return repo.GetEmpById(id);
        }

        public void DeleteTime(TimeEntry entry)
        {
            EmployeeRepo repo = new EmployeeRepo();

            repo.DeleteTime(entry);
        }
    }
}
