﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data;
using Flooring.Models;

namespace Flooring.BLL
{
    public class StateTaxManager
    {
        private bool production;
        public StateTaxManager(bool config)
        {
            production = config;
        }
        public StateTaxManager(string config)
        {
            if (config == "true")
            {
                production = true;
            }
            else
            {
                production = false;
            }
        }
        public decimal GetTaxRate(string state)
        {
            IDataSelector data;
            if (production)
            {
                data = new ProdDB("", "taxes");
            }
            else
            {
                data = new MockDB("", "taxes");
            }
            string[] lines = data.GetData().ToArray();
            for (int i = 0; i < lines.Length; i++)
            {
                string[] columns = lines[i].Split(',');
                if (state == columns[0] || state == columns[1])
                {
                    return decimal.Parse(columns[2]);
                }
            }
            return -1.00M;
        }

        public bool CheckStateTax(string state)
        {
            if (GetTaxRate(state) < 0)
            {
                return false;
            }
            return true;
        }
    }
}
