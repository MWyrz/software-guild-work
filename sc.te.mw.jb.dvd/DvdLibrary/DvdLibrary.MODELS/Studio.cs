﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DvdLibrary.MODELS
{
    public class Studio
    {
        public int StudioId { get; set; }
        [Required(ErrorMessage = "Please Enter The Studio Name")]
        public string StudioName { get; set; }
    }
}
