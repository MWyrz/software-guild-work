﻿/// <reference path="../jquery-2.2.1.js" />

var msgPostUri = '/api/Messages/';

$(document).ready(function () {
    $(document).on("click", ".btnSendMsg", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        var newMsg = {};

        newMsg.isRead = 0;
        newMsg.FirstName = $('#FirstName').val();
        newMsg.LastName = $('#LastName').val();
        newMsg.EmailAddress = $('#EmailAddress').val();
        newMsg.MessageBody = $('#MessageBody').val();
        if ($("#SendMsgForm").valid()) {
            $.post(msgPostUri + 'post/', newMsg).success(function (data) {
                window.location.href = "/Home/Index";
            }).error(function (e, status) {
                console.log(e);
            }).done(function () {
            });
        }
        
    });
});
