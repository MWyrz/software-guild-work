﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSRM.Capstone.BLL;
using MSRM.Capstone.Data;
using MSRM.Capstone.Model;
using NUnit.Framework;

namespace MSRM.Capstone.Test
{
    [TestFixture]
    public class MessageTests
    {
        private MessageRepository _msgRepo;
        private Message _testMsg;

        [SetUp]
        public void TestSetUp()
        {
            _msgRepo = new MessageRepository();
            _testMsg = new Message();
            Utilities.RebuildTestDb();
        }

        [Test]
        public void CanAddMsg()
        {
            _testMsg.FirstName = "John";
            _testMsg.LastName = "Smith";
            _testMsg.MessageBody = "Test Message";
            _testMsg.Date = DateTime.Parse("March 21, 2016");
            _testMsg.EmailAddress = "jsmith1@yahoo.com";

            var manager = new MessageManager();

            var response = manager.CreateMessage(_testMsg);

            Assert.IsTrue(response.Success);
        }

        [Test]
        public void CanAddReadMsg()
        {
            _testMsg.FirstName = "Jack";
            _testMsg.LastName = "Bauer";
            _testMsg.MessageBody = "Test Message";
            _testMsg.Date = DateTime.Parse("March 1, 2016");
            _testMsg.EmailAddress = "azzkikr1@yahoo.com";
            _testMsg.isRead = 1;

            var manager = new MessageManager();

            var response = manager.CreateMessage(_testMsg);

            Assert.IsTrue(response.Success);
        }

        [Test]
        public void GetAllMsgs()
        {
            var msgList = _msgRepo._messageList;

            Assert.AreEqual(2, msgList.Count);
        }
    }
}
