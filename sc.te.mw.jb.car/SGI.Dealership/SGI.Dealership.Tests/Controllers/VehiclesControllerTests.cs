﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using SGI.Dealership.Controllers;
using SGI.Dealership.Models;

namespace SGI.Dealership.Tests.Controllers
{
    [TestFixture]
    class VehiclesControllerTests
    {
        private Mock<DbSet<Vehicle>> _mockSet;
        private Mock<SgiDealershipContext> _mockContext;
        private VehiclesController _controller;


        [SetUp]
        public void TestSetUp()
        {
            //Arrange
            _mockSet = new Mock<DbSet<Vehicle>>();
            _mockContext = new Mock<SgiDealershipContext>();
            _mockContext.Setup(m => m.Vehicles).Returns(_mockSet.Object);
            _controller = new VehiclesController(_mockContext.Object);
        }

        [Test]
        public void CanAddVehicle()
        {
            //Act
            _controller.PostVehicle(new Vehicle
            {
                Make = "Subaru", Model="Impreza", Year = 2016, Condition = "New", Mileage = 22,
                Price = 28000M, AdTitle = "Brand New 2016 Impreza", Description = "Heated Seats"
            });

            //Assert
            _mockSet.Verify(m => m.Add(It.IsAny<Vehicle>()), Times.Once());
            _mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }
    }
}
