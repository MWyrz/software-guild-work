﻿namespace BaseballLeague.MODELS
{
    public class Team
    {
        public int TeamId { get; set; }
        public int LeagueId { get; set; }
        public string TeamName { get; set; }
        public int ManagerId { get; set; }
    }
}